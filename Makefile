OBJDIR=./obj/

CC=gcc
LDFLAGS= -lpigpio -lrt
CFLAGS= -Wall -Wextra -pedantic -std=c99

OBJ_nogpio=t_ttbasic.o t_kilo.o nogpio.o
OBJ_rpi=t_ttbasic.o t_kilo.o rpi_gpio.o

OBJS_nogpio = $(addprefix $(OBJDIR), $(OBJ_nogpio))
OBJS_rpi = $(addprefix $(OBJDIR), $(OBJ_rpi))
DEPS = Makefile tanba.h


all: $(OBJDIR) nogpio

nogpio: $(OBJS_nogpio)
	$(CC) $(CFLAGS) $^ -o tanbabasic


rpi: $(OBJDIR) rpi_

rpi: CFLAGS+= -DRPI

rpi_: $(OBJS_rpi)
	$(CC) $(CFLAGS) $^ -o tanbabasic $(LDFLAGS)

$(OBJDIR)%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR):
	mkdir -p $(OBJDIR)

.PHONY: clean

clean:
	rm -rf $(OBJS_nogpio) $(OBJS_rpi) tanbabasic
