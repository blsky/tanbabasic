// Tanba Labs BASIC
//   Based on TOYOSHIKI's TinyBASC implementation

// Original TOYOSHIKI's license is GPL as stated on the link below:
//   https://vintagechips.wordpress.com/2015/12/06/%E8%B1%8A%E5%9B%9B%E5%AD%A3%E3%82%BF%E3%82%A4%E3%83%8B%E3%83%BCbasic%E7%A2%BA%E5%AE%9A%E7%89%88/
// vintagechips より:
// 2018年10月29日 10:26 AM
//   Hi Rifath Shaarook, Thank you for your comment. Toyoshiki Tiny BASIC (for short, ‘ttbasic’) is released under the GPL license. Please use freely according to GPL. I hope that a good educational outcome will be born.

#define _POSIX_C_SOURCE 199309L

// Compiler requires description
#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#define __USE_XOPEN
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include "tanba.h"
#include <errno.h>

char fileName[256];
FILE *b_file;

int gpioMode;
int gpioIn[    MAX_GPIO];
int gpioInMode[MAX_GPIO];
int gpioOut[   MAX_GPIO];
int gpioPwm[   MAX_GPIO];
int gpioServo_[ MAX_GPIO];

int outMode; // output mode


int adcModel=-1; // not defined

                       
                   // MCP  3002     3004     3008     3202     3204     3208
int AdcBytes[       ]={       2,       3,       3,       3,       3,       3};
int AdcBits[        ]={      10,      10,      10,      12,      12,      12};
int AdcInitBuf[     ]={0x600000,0x018000,0x018000,0x018000,0x060000,0x060000}; // start bit and single mode
int AdcChannels[    ]={       2,       4,       8,       2,       4,       8};
int AdcChannelShift[]={      20,      12,      12,      14,      14,      14};

char *color_names[] = {
  "BLACK",
  "BLUE",
  "BROWN",
  "CYAN",
  "DARKBLUE",
  "DARKGREEN",
  "DARKRED",
  "DARKVIOLET",
  "DEEPPINK",
  "GOLD",
  "GRAY",
  "GREEN",
  "LIME",
  "MAGENTA",
  "MAROON",
  "ORANGE",
  "ORANGERED",
  "PINK",
  "PURPLE",
  "RED",
  "VIOLET",
  "WHITE",
  "YELLOW",
  "HL",
  "HIGHLIGHT"
};

// Color names count
#define SIZE_COLOR (sizeof(color_names) / sizeof(const char*))

int color_codes[] = {
   16, // BLACK
   21, // BLUE
  131, // BROWN
   51, // CYAN
   19, // DARKBLUE
   28, // DARKGREEN
  124, // DARKRED
  128, // DARKVIOLET
  199, // DEEPPINK
  220, // GOLD
  145, // GRAY
   34, // GREEN
   46, // LIME
  201, // MAGENTA
  124, // MAROON
  214, // ORANGE
  202, // ORANGERED
  224, // PINK
  127, // PURPLE
  196, // RED
  219, // VIOLET
  231, // WHITE
  226, // YELLOW
  256, // HL
  256  // HIGHLIGHT
};


short waittime_cs = 1;

short unget = 0; // unfortunatelly ungetc does not work for read.


void c_putch(char c){
  if (b_file) {
    fprintf(b_file,"%c",c);
  } else {
    char line[2];
    line[0]=c;
    line[1]=0;
    basicLine(line);
    editorRefreshScreen();
  }
}

void gpioCfgError(char *fileName, char *line){
	printf("Wrong line in gpio config file:%s\n",fileName);
	printf("Line:%s\n",line);
	exit(-1);
}

void configGpio(char *fileName){
#if defined(RPI) //  || defined(...)

	gpioMode=-1;
	memset(gpioIn    ,     -1,MAX_GPIO*sizeof(int));
	memset(gpioInMode,PULLDEF,MAX_GPIO*sizeof(int));
	memset(gpioOut   ,     -1,MAX_GPIO*sizeof(int));
	memset(gpioPwm   ,     -1,MAX_GPIO*sizeof(int));
	memset(gpioServo_,     -1,MAX_GPIO*sizeof(int));

        outMode=0;

	FILE *gf=fopen(fileName,"r");
	if (gf==NULL) {
		printf("ERROR:Could not open config file %s\n",fileName);
		exit(-1);
	}
	char line[64];
	char c;
	char *p;
	int gpioOrPinNum;
	int inNum;
	int outNum;
	int firstLine=1;
	int inOutStarted=0;
	while(1){ 
		p=fgets(line,64,gf);
		if (p==NULL) break;
		p[strlen(p)-1]='\0'; // remove newline
//printf("NEXT line %d %s   fist %d\n",strlen(p),p,firstLine);
		gpioOrPinNum=-1;
		inNum=-1;
		outNum=-1;
		while(*p) {
			if (*p=='#') break;
			if (*p!=' ' && *p!='\t') {
				if        (firstLine) {
					if (strncmp(p,"BOARD",5)==0) {
						gpioMode=GPIO_BOARD;
						p=p+5;
					} else if (strncmp(p,"BCM"  ,3)==0) {
						gpioMode=GPIO_BCM;
						p=p+3;
					}
					if (gpioMode!=-1) {
						// find optional OUT_MODE
						int foundOutMode=0;
						while(*p) {
							if (*p=='#') break;
							if (*p!=' ' && *p!='\t') {
								if (strncmp(p,"OUT_MODE",8)==0) {
									p=p+8;
									while(*p) {
										if (*p=='#') gpioCfgError(fileName,line);
										if (*p!=' ' && *p!='\t') {
											outMode=atoi(p);
											if (outMode>=MAX_GPIO) {
												printf("max OUT_MODE is \n",MAX_GPIO-1);  
												gpioCfgError(fileName,line);
											}
											foundOutMode=1;
											break;

										}
										p++;
									}
								} else {
									printf("wrong 1st valid line\n");
									gpioCfgError(fileName,line);
								}
								if (foundOutMode) break;
							}
							p++;
						}
						firstLine=0;
					}
				} else {
					if (inOutStarted==0) { // in/out definition not started yet.
								// try to find other configs.	
						if (strncmp(p,"ADC_MAIN",8)==0) {
							p=p+8;
							while(*p) {
								if (*p=='#') gpioCfgError(fileName,line);
								if (*p!=' ' && *p!='\t') {
							  
									if (       strncmp(p,"MCP3002",7)==0) {
										adcModel=MCP3002;
									} else if (strncmp(p,"MCP3004",7)==0) {
										adcModel=MCP3004;
									} else if (strncmp(p,"MCP3008",7)==0) {
										adcModel=MCP3008;
									} else if (strncmp(p,"MCP3202",7)==0) {
										adcModel=MCP3202;
									} else if (strncmp(p,"MCP3204",7)==0) {
										adcModel=MCP3204;
									} else if (strncmp(p,"MCP3208",7)==0) {
										adcModel=MCP3208;
									} else {
										printf(" Not supported ADC model\n");
										gpioCfgError(fileName,line);
									}
									break;
								}
								p++;
							}
							break;
						}
					}
					gpioOrPinNum=atoi(p);
					if (gpioOrPinNum<0 || gpioOrPinNum>=MAX_GPIO) {
						printf("gpioOrPinNum out of range 0-%d\n",MAX_GPIO-1);  
						gpioCfgError(fileName,line);
					}
				}
				break;
			}
			p++;
		}
//printf("GPIO %d  model %d\n",gpioOrPinNum,adcModel);
		if (gpioOrPinNum>=0) { // has line)
			inOutStarted=1;
			
			if (adcModel>=0) {
				int gpioNum_=checkAndGetGpioNum();
				
				if ((gpioMode==GPIO_BCM   && gpioOrPinNum>=8 && gpioOrPinNum<=11) ||
				    (gpioMode==GPIO_BOARD && (gpioOrPinNum==19 || gpioOrPinNum==21 || gpioOrPinNum==23 || gpioOrPinNum==24))) { 
					printf("Cannot set GPIOs 8-11 (pins 19,21,23,24) because using SPI for ADC chip\n");
					gpioCfgError(fileName,line);
				}
			}
			
			int a;
			for (a=0;a<MAX_GPIO;a++) {
				if (gpioIn[a]==gpioOrPinNum||gpioOut[a]==gpioOrPinNum||gpioPwm[a]==gpioOrPinNum||gpioServo_[a]==gpioOrPinNum) {
					printf("Gpio already in use gpio %d (In %d, out %d, pwm %d, servo)\n",gpioOrPinNum,gpioIn[a],gpioOut[a],gpioPwm[a],gpioServo_[a]);
					gpioCfgError(fileName,line);
				}
			}
			char *p1=strchr(p,',');
			if (p1==NULL) {
				gpioCfgError(fileName,line);
			}
			p=p1+1; // skip comma
//printf("comma %s\n",p);
			int portNum=-1;
			while(*p) {
				if (*p=='#') gpioCfgError(fileName,line);
				if (*p!=' ' && *p!='\t') {
					int portMode;
					if        (strncmp(p,"IN",2)==0) {
						p=p+2;
						portMode=IN_MODE;
					} else if (strncmp(p,"OUT",3)==0) {
						p=p+3;
						portMode=OUT_MODE;
					} else if (strncmp(p,"PWM",3)==0) {
						p=p+3;
						portMode=PWM_MODE;
					} else if (strncmp(p,"SERVO",5)==0) {
						p=p+5;
						portMode=SERVO_MODE;
					} else {
						gpioCfgError(fileName,line);
					}
					if(*p>='0' && *p<='9') {
						portNum=atoi(p);
						if (portNum<0 || portNum>=MAX_GPIO) {
							printf("portNum out of range 0-%d\n",MAX_GPIO);  
							gpioCfgError(fileName,line);
						}
					} else {
						while(*p) {
							if (*p=='#') {
								*p=0;
								break;
							}
							if (*p==',' && portMode==IN_MODE) break;
							if (*p!=' ' && *p!='\t') {
								gpioCfgError(fileName,line);
							}
							p++;
						}
					}
					if (portNum<0) portNum=gpioOrPinNum;
					if ((portMode==IN_MODE    && gpioIn[   portNum]>=0) ||
					    (portMode==OUT_MODE   && gpioOut[  portNum]>=0) ||
					    (portMode==PWM_MODE   && gpioPwm[  portNum]>=0) ||
					    (portMode==SERVO_MODE && gpioServo_[portNum]>=0)) {
						printf("Port already in use %d (In %d, out %d, pwm %d, servo %d)\n",portNum,gpioIn[portNum],gpioOut[portNum],gpioPwm[portNum],gpioServo_[portNum]);
						gpioCfgError(fileName,line);
					}
//printf("%s\n",line);					
//printf("INflag %d  port %d  num %d %s\n",inFlag,portNum,gpioOrPinNum,p);
					if (portMode==IN_MODE) {
						p1=strchr(p,',');
						int inMode=PULLDEF;
						if (p1!=NULL) {
							p=p1+1; // skip comma
							while(*p==' ' || *p=='\t') {
								p++;
							}
							if        (strncmp(p,"PULLDEF" ,7)==0) {
								inMode=PULLDEF;
							} else if (strncmp(p,"PULLOFF" ,7)==0) {
								inMode=PULLOFF;
							} else if (strncmp(p,"PULLUP"  ,6)==0) {
								inMode=PULLUP;
							} else if (strncmp(p,"PULLDOWN",8)==0) {
								inMode=PULLDOWN;
							} else {
								printf("Wrong Input PULL mode in config file |%s|\n",p);
								gpioCfgError(fileName,line);
							}
//printf("GPIO INPUT mode p %d  gpio %d  mode %d\n",portNum,gpioOrPinNum,inMode);							
						}
						gpioIn[    portNum]=gpioOrPinNum;
						gpioInMode[portNum]=inMode;
					} else if (portMode==OUT_MODE){
						gpioOut[portNum]=gpioOrPinNum;
					} else if (portMode==PWM_MODE){
						gpioPwm[portNum]=gpioOrPinNum;
					} else {          // SERVO_MODE
						gpioServo_[portNum]=gpioOrPinNum;
					}
				}
				if (portNum>=0) break;
				p++;
			}
			if(portNum<0) {
				gpioCfgError(fileName,line);
			}
		}
	}

#if defined(RPI)
	if (gpioMode==-1) {
		printf("ERROR: For RPI, 1st config in cfg file (%s) shall be GPIO mode (BOARD or BCM)\n");
		exit(-1);
	}
	
#endif
#else
  printf("No GPIO. IN and OUT commands will have no effect.\n");
  sleep(1);
#endif
	checkPort();
}

// STDIN_FILENO blocking issue
//   ============================
//   Oritinal non-blocking input implementation:
//       kilo editor uses tcsetattr VMIN/VTIME.
//       ttbasic     uses fcntl O_NONBLOCK 
// As link below, O_NONBLOCK has precedence over VMIN/VTIME
//    https://stackoverflow.com/questions/25996171/linux-blocking-vs-non-blocking-serial-read
// On this software, originally, O_NONBLOCK was set/unset only during c_kbhit(now changed to c_check_esc)).
// However, even so, I found that the behaviour of VMIN/VTIME differs as below:
//   Ubuntu 18.04 x64: Works on block mode only, even with the above set.
//   Raspberry pi OS : Works on non-blocking mode (worked fine for INKEY, but not for INPUT).
// So, there are two issues:
//   Issue1: works differently on Ubuntu/Raspberry
//   Issue2: works differently for INKEY/INPUT on raspberry
// I had to adjust the code to make it work properly.

short c_getch(){
	struct termios b;
	struct termios a;
	unsigned char c;

	tcgetattr(STDIN_FILENO, &b);
	a = b;
	a.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &a);
	c = getchar();

	if (c == '\x1b') { // escape
		int f;

		f = fcntl(STDIN_FILENO, F_GETFL, 0);
		fcntl(STDIN_FILENO, F_SETFL, f | O_NONBLOCK);

		char c1=getchar();
		if (c1==0x5b) {
			char c2=getchar();
			switch (c2){
				case 0x41: // up
					//return 30;
					c=30;
					break;
				case 0x42: // down
					//return 31;
					c=31;
					break;
				case 0x43: // right
					//return 29;
					c=29;
					break;
				case 0x44: // left
					//return 28;
					c=28;
					break;
			}

		} 
		fcntl(STDIN_FILENO, F_SETFL, f);
	}
	tcsetattr(STDIN_FILENO, TCSANOW, &b);

	return c&255;
}

short  c_getch_nonblock(){
	short c;
	int f;

	f = fcntl(STDIN_FILENO, F_GETFL, 0);
	fcntl(STDIN_FILENO, F_SETFL, f | O_NONBLOCK);

	c = c_getch();

	fcntl(STDIN_FILENO, F_SETFL, f);
        return c;
}

short get_last_key(){
	int f;

	f = fcntl(STDIN_FILENO, F_GETFL, 0);
	fcntl(STDIN_FILENO, F_SETFL, f | O_NONBLOCK);

        char cc[1024];
        int nread = read(STDIN_FILENO, cc, 1024);

	fcntl(STDIN_FILENO, F_SETFL, f);
	short c=255;

	if (nread>=3) {
	      	//  when we hold a key pressed, it will fill in stdin buffer, until we read them
		//  here, we want the last key pressed only.

		if (cc[nread-3]==0x1b && cc[nread-2]==0x5b) { // arrow
			char c2=cc[nread-1];
			switch (c2){
				case 0x41: // up
					c=30;
					break;
				case 0x42: // down
					c=31;
					break;
				case 0x43: // right
					c=29;
					break;
				case 0x44: // left
					c=28;
					break;
			}

		} 
	}
	if (c==255) {
		if (nread>0) {
			c=cc[nread-1];
		} else if (unget!=0) {
			c=unget;
		}
	}
	unget=0;
	return c;
}

// former c_kbhit
char c_check_esc(void) {
	short c; int f;

//	c = c_getch_nonblock();
	c = get_last_key();

	
	if (c != EOF && c!=255) {
		if (c==0x1b)	return 1;
		unget=c;
	}

	return 0;
}

void newline(void){
	if (b_file) {
		fprintf(b_file,"\n");
	} else {
		basicNewline(NULL);
	}
}

// Return random number
short getrnd(short value){
        return(rand() % value) + 1;
}


short getinkey() {

	short c=get_last_key();

	if (c==0x1b) { // escape
		unget=c;
	}
	c= c==255 ? 0:c;
	
	return c;
}
  
short getyear(){
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
        return tm.tm_year + 1900;
}

short getmonth(){
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
        return tm.tm_mon + 1;
}

short getday(){
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
        return tm.tm_mday;
}

short gethour(){
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
        return tm.tm_hour;
}

short getminute(){
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
        return tm.tm_min;
}

short getsecond(){
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
        return tm.tm_sec;
}

// Prototypes (necessity minimum)
short iexp(void);

// Keyword table
const char* kwtbl[] = {
	"GOTO", "GOSUB", "RETURN",
	"FOR", "TO", "STEP", "NEXT",
	"IF", "ELSE","REM", "STOP",
	"INPUT", "PRINT", "LET",
	",", ":",
	"-", "+", "*", "/", "%", "(", ")",
	">=", "#", ">", "=", "<=", "<",
	 "@", "RND", "ABS", "SIZE",
	"LIST", "RUN", "NEW", "SYSTEM",
	"?","WAIT","INKEY","CLS","COLOR","SGR",
        "YEAR","MONTH","DAY","HOUR","MINUTE","SECOND",
	"IN","OUT","LED","PWM","SERVO","GPIO","ANA",
	"LOCATE","LC","RESIZE","SAVE","LOAD","END",
	"OK","YOU","SYNTAX"
};

// i-code(Intermediate code) assignment
enum{
	I_GOTO, I_GOSUB, I_RETURN,      //  0, 1, 2
	I_FOR, I_TO, I_STEP, I_NEXT,    //  3, 4, 5,6
	I_IF, I_ELSE, I_REM, I_STOP,            //  7, 8, 9, 10
	I_INPUT, I_PRINT, I_LET,        // 11,12,13
	I_COMMA, I_COLON,               // 14,15
	I_MINUS, I_PLUS, I_MUL, I_DIV, I_REMINDER, I_OPEN, I_CLOSE, // 16,17,18,19,20,21,22
	I_GTE, I_SHARP, I_GT, I_EQ, I_LTE, I_LT, // 23,24,25,26,27,28
	I_ARRAY, I_RND, I_ABS, I_SIZE,           // 29,30,31,32
	I_LIST, I_RUN, I_NEW, I_SYSTEM,          // 33,34,35,36
	I_QUESTION,I_WAIT,I_INKEY,I_CLS,         // 37,38,39,40
	I_COLOR,I_SGR,                           // 41,42
        I_YEAR,I_MONTH,I_DAY,I_HOUR,I_MINUTE,I_SECOND, // 43,44,45,46,47,48
	I_IN,I_OUT,I_LED,I_PWM,I_SERVO,I_GPIO,I_ANA,   // 49,50,51,52,53,54,55
	I_LOCATE,I_LC,I_RESIZE,I_SAVE,I_LOAD,I_END,    // 56,57,58,59,60,61
	I_OK,I_YOU,I_SYNTAX,           // 62,63,64 Keys UP TO HERE!
	I_NUM, I_VAR, I_STR,           // 65,66,67
	I_BIN, I_0B, I_0X, I_COLCODE,  // 68,69,70,71
	I_EOL                          // 72
	};

// Keyword count
#define SIZE_KWTBL (sizeof(kwtbl) / sizeof(const char*))

// List formatting condition
// no space after
const unsigned char i_nsa[] = {
	I_RETURN, I_STOP, I_COMMA,
	I_MINUS, I_PLUS, I_MUL, I_DIV, I_REMINDER, I_OPEN, I_CLOSE,
	I_GTE, I_SHARP, I_GT, I_EQ, I_LTE, I_LT,
	I_ARRAY, I_RND, I_ABS, I_SIZE,I_INKEY,I_IN,I_ANA,I_END,
	I_OK,I_YOU,I_SYNTAX
};

// no space before (after numeric or variable only)
const unsigned char i_nsb[] = {
	I_MINUS, I_PLUS, I_MUL, I_DIV, I_REMINDER, I_OPEN, I_CLOSE,
	I_GTE, I_SHARP, I_GT, I_EQ, I_LTE, I_LT,
	I_COMMA, I_COLON, I_EOL
};

// exception search function
char sstyle(unsigned char code, const unsigned char *table, unsigned char count) {
	while(count--)
		if (code == table[count])
			return 1;
	return 0;
}

// exception search macro
#define nospacea(c) sstyle(c, i_nsa, sizeof(i_nsa))
#define nospaceb(c) sstyle(c, i_nsb, sizeof(i_nsb))

// Error messages
unsigned char err;// Error message index
const char* errmsg[] ={
	"OK",
	"Devision by zero",
	"Overflow",
	"Subscript out of range",
	"Icode buffer full(line too long)",
	"List full",
	"GOSUB too many nested",
	"RETURN stack underflow",
	"FOR too many nested",
	"NEXT without FOR",
	"NEXT without counter",
	"NEXT mismatch FOR",
	"FOR without variable",
	"FOR without TO",
	"LET without variable",
	"IF without condition",
	"Undefined line number",
	"\'(\' or \')\' expected",
	"\'=\' expected",
	"File not found",
	"Illegal command",
	"SYNTAX ERROR",
	"Internal error",
	"Abort by [ESC]"
};

// Error code assignment
enum{
	ERR_OK,
	ERR_DIVBY0,
	ERR_VOF,
	ERR_SOR,
	ERR_IBUFOF, ERR_LBUFOF,
	ERR_GSTKOF, ERR_GSTKUF,
	ERR_LSTKOF, ERR_LSTKUF,
	ERR_NEXTWOV, ERR_NEXTUM, ERR_FORWOV, ERR_FORWOTO,
	ERR_LETWOV, ERR_IFWOC,
	ERR_ULN,
	ERR_PAREN, ERR_VWOEQ,
	ERR_LOAD,
	ERR_COM,
	ERR_SYNTAX,
	ERR_SYS,
	ERR_ESC,
	ERR_FILE
};


// RAM mapping
char lbuf[SIZE_LINE]; //Command line buffer
unsigned char ibuf[SIZE_IBUF]; //i-code conversion buffer
short var[26]; //Variable area
short arr[SIZE_ARRY]; //Array area
unsigned char listbuf[SIZE_LIST]; //List area
unsigned char* clp; //Pointer current line
unsigned char* cip; //Pointer current Intermediate code
unsigned char* gstk[SIZE_GSTK]; //GOSUB stack
unsigned char gstki; //GOSUB stack index
unsigned char* lstk[SIZE_LSTK]; //FOR stack
unsigned char lstki; //FOR stack index

// Standard C libraly (about) same functions
char c_toupper(char c) {return(c <= 'z' && c >= 'a' ? c - 32 : c);}
char c_isprint(char c) {return(c >= 32  && c <= 126);}
char c_isspace(char c) {return(c == ' ' || (c <= 13 && c >= 9));}
char c_isdigit(char c) {return(c <= '9' && c >= '0');}
char c_isalpha(char c) {return ((c <= 'z' && c >= 'a') || (c <= 'Z' && c >= 'A'));}
//void c_puts(const char *s) {while(*s) c_putch(*s++);}
void c_puts(const char *s) {
  if (b_file) {
    fprintf(b_file,"%s",s);
  } else {
    basicLine(s);
  }
}

void c_convert_cmd(char *cmd){
  // based on original c_gets()
	char c;
	unsigned char len;
        if (cmd!=NULL) {
		len = strlen(cmd);

		strncpy(lbuf,cmd,SIZE_LINE);
                if (len<SIZE_LINE) {		
			lbuf[len] = 0; // Put NULL
		} else {
			lbuf[SIZE_LINE-1] = 0; // Put NULL
		}
		if(len > 0){
			while(c_isspace(lbuf[--len])); // Skip space
			lbuf[++len] = 0; // Put NULL
		}
 	}
}


// Print numeric specified columns
void putnum(short value, short d){
	unsigned char i;
	unsigned char sign;

	int ivalue=value; // if short, will not work for -32768

	if(ivalue < 0){
		sign = 1;
		ivalue = -ivalue;
	} else {
		sign = 0;
	}

	lbuf[6] = 0;
	i = 6;
	do {
		lbuf[--i] = (ivalue % 10) + '0';
		ivalue /= 10;
	} while(ivalue > 0);

	if(sign)
		lbuf[--i] = '-';

	//String length = 6 - i
	while(6 - i < d){ // If short
		c_putch(' '); // Fill space
		d--;
	}
	c_puts(&lbuf[i]);
}

// Input numeric and return value
// Called by only INPUT statement
short getnum(){
	short value, tmp;
	short c;
	unsigned char len;
	unsigned char sign;

	int mode=I_NUM;

	len = 0;
	while(1){
		disableRawMode();
		c = c_getch();
		enableRawMode();
		if (c==10) break; // 10 in this mode is enter
		char c_ = c_toupper(c);
		if (c == 27) { // ESC ?
			err = ERR_ESC;
			return NULL;
		} else
		if(((c == 8) || (c == 127)) && (len > 0)){ // Backspace manipulation
			len--;
			//c_putch(8); c_putch(' '); c_putch(8);
			editorDelChar();c_putch(' ');editorDelChar();
			editorRefreshScreen();
		} else
		if( (len == 0 && (c == '+' || c == '-')) ||
			(len == 0 && c == '`') ||                              // bin
			(len == 1 && lbuf[0] =='0' && (c_ == 'B') || ( c_ == 'X') )||  // bin/hex
			(mode==I_NUM && len <  6 && c_isdigit(c)) || // Numeric
			(mode==I_BIN && len < 17 && (c=='0' || c=='1')) || 
			(mode==I_0B  && len < 18 && (c=='0' || c=='1')) || 
			(mode==I_0X  && len <  6 && (c_isdigit(c) || (c_>='A' && c_ <='F')))){ 
			lbuf[len++] = c;
			c_putch(c);
			if (          c =='`') { mode=I_BIN; }
			if (c_toupper(c)=='B') { mode=I_0B; }
			if (c_toupper(c)=='X') { mode=I_0X; }
		}
	}
	newline();
	lbuf[len] = 0;

	switch(mode) {
	case I_NUM:
		switch(lbuf[0]){
		case '-':
			sign = 1;
			len = 1;
			break;
		case '+':
			sign = 0;
			len = 1;
			break;
		default:
			sign = 0;
			len = 0;
			break;
		}

		value = 0; // Initialize value
		tmp = 0; // Temp value
		while(lbuf[len]){
			tmp = 10 * value + lbuf[len++] - '0';
			if(value > tmp){ // It means overflow
				err = ERR_VOF;
			}
			value = tmp;
		}
		if(sign)
			return -value;
		return value;
		break;
	case I_BIN:
	case I_0B:
		sign = 0;
		if (mode==I_BIN) {
			len = 1;
		} else {
			len = 2;
		}
		value=0;
		while(lbuf[len]){
			value = 2 * value + lbuf[len++] - '0';
		}
		return value;
		break;
	case I_0X:
		sign = 0;
		len = 2;
		value=0;
		while(lbuf[len]){
			short xcode=c_toupper(lbuf[len++]);
			if   (c_isdigit(xcode))  {xcode=xcode - '0';}
			else                     {xcode=xcode - 'A'+10;}
			value = 16 * value + xcode;
		}		
		return value;
		break;
	}
}

// Convert token to i-code
// Return byte length or 0
unsigned char toktoi() {
	unsigned char i; // Loop counter(i-code sometime)
	unsigned char len = 0; //byte counter
	char* pkw = 0; // Temporary keyword pointer
	char* ptok; // Temporary token pointer
	char* s = lbuf; // Pointer to charactor at line buffer
	char c; // Surround the string character, " or '
	short value; //numeric
	short tmp; //numeric for overflow check
	while (*s) {
		while (c_isspace(*s)) s++; // Skip space

		//Try keyword conversion
		for (i = 0; i < SIZE_KWTBL; i++) {
			pkw = (char *)kwtbl[i]; // Point keyword
			ptok = s; // Point top of command line
			// Compare 1 keyword
			while ((*pkw != 0) && (*pkw == c_toupper(*ptok))) {
				pkw++;
				ptok++;
			}

			if (*pkw == 0) {// Case success

				if (len >= SIZE_IBUF - 1) {// List area full
					err = ERR_IBUFOF;
					return 0;
				}
				// i have i-code
				ibuf[len++] = i;
				s = ptok;
				break;
			}
		}
		// Case statement needs an argument except numeric, valiable, or strings
		if(i == I_REM || i==I_OK || i==I_YOU || i==I_SYNTAX) {
			while (c_isspace(*s)) s++; // Skip space
			ptok = s;
			for (i = 0; *ptok++; i++); // Get length
			if (len >= SIZE_IBUF - 2 - i) {
				err = ERR_IBUFOF;
				return 0;
			}
			ibuf[len++] = i; // Put length
			while (i--) { // Copy strings
		 		ibuf[len++] = *s++;
			}
			break;
		}

		if (*pkw == 0)
			continue;

		ptok = s; // Point top of command line

		// Try binary conversion
		if (*s == '`' || (s[0]=='0' && c_toupper(s[1])=='B')) {// If start of binary
			s++;
			char code=I_BIN;
			if         (c_toupper(*s)=='B') {
				s++;
				code=I_0B;
			}

			ptok = s;
 			for (i = 0; (c_isdigit(*ptok) || (c_toupper(*ptok)>='A' && c_toupper(*ptok)<='F')); i++) // Get length
				ptok++;
			if (len >= SIZE_IBUF - 1 - i) { // List area full
				err = ERR_IBUFOF;
				return 0;
			}
			ibuf[len++] = code; // Put i-code
			ibuf[len++] = i; // Put length
			while (i--) { // Put string
				ibuf[len++] = c_toupper(*s++);
			}
		}
		else

		// Try hex conversion
		if (s[0]=='0' &&  c_toupper(s[1])=='X') {// If start of hex
			s++;
			s++;
			ptok = s;

 			for (i = 0; (c_isdigit(*ptok) || (c_toupper(*ptok)>='A' && c_toupper(*ptok)<='F')); i++) // Get length
				ptok++;
			if (len >= SIZE_IBUF - 1 - i) { // List area full
				err = ERR_IBUFOF;
				return 0;
			}
			ibuf[len++] = I_0X; // Put i-code
			ibuf[len++] = i; // Put length
			while (i--) { // Put string
				ibuf[len++] = *s++;
			}
		}
		else

		// Try numeric conversion
		if (c_isdigit(*ptok)) {
			value = 0;
			tmp = 0;
			do {
				tmp = 10 * value + *ptok++ - '0';
				if (value > tmp) {
					err = ERR_VOF;
					return 0;
				}
				value = tmp;
			} while (c_isdigit(*ptok));

			if (len >= SIZE_IBUF - 3) {
				err = ERR_IBUFOF;
				return 0;
			}
			ibuf[len++] = I_NUM;
			ibuf[len++] = value & 255;
			ibuf[len++] = value >> 8;
			s = ptok;
		}
		else

		// Try string conversion
		if (*s == '\"' || *s == '\'') {// If start of string
			c = *s++;
			ptok = s;
			for (i = 0; (*ptok != c) && c_isprint(*ptok); i++) // Get length
				ptok++;
			if (len >= SIZE_IBUF - 1 - i) { // List area full
				err = ERR_IBUFOF;
				return 0;
			}
			ibuf[len++] = I_STR; // Put i-code
			ibuf[len++] = i; // Put length
			while (i--) { // Put string
				ibuf[len++] = *s++;
			}
			if (*s == c) s++; // Skip " or '
		}
		else


		// Try valiable conversion .. tiny basic variable names: 1 char only...
		if (c_isalpha(*ptok) && c_isalpha(ptok[1])==0) {
			if (len >= SIZE_IBUF - 2) {
				err = ERR_IBUFOF;
				return 0;
			}
			if (len >= 4 && ibuf[len - 2] == I_VAR && ibuf[len - 4] == I_VAR) { // Case series of variables
				err = ERR_SYNTAX; // Syntax error
				return 0;
			}
			ibuf[len++] = I_VAR; // Put i-code
			ibuf[len++] = c_toupper(*ptok) - 'A'; // Put index of valiable area
			s++;



		}
		else

		// Try other code matching
		{
			int foundFlag=0;
			for (i = 0; i < SIZE_COLOR; i++) {
				pkw = (char *)color_names[i]; // Point color names
				ptok = s; // Point top of command line
				// Compare 1 keyword
				while ((*pkw != 0) && (*pkw == c_toupper(*ptok))) {
					pkw++;
					ptok++;
				}

				if (*pkw == 0) {// Case success

					if (len >= SIZE_IBUF - 1) {// List area full
						err = ERR_IBUFOF;
						return 0;
					}
					// i have i-code
					ibuf[len++] = I_COLCODE;
					ibuf[len++] = i;
					s = ptok;
					foundFlag=1;
		 			break;
				}
			}
			if (foundFlag==0) {

				err = ERR_SYNTAX;
				return 0;
			}
		}
	}
	ibuf[len++] = I_EOL; // Put end of line
	return len; // Return byte length
}

// Get line numbere by line pointer
short getlineno(unsigned char *lp) {
	if(*lp == 0) //end of list
		return 32767;// max line bumber
	return *(lp + 1) | *(lp + 2) << 8;
}

// Search line by line number
unsigned char* getlp(short lineno) {
	unsigned char *lp;

	for (lp = listbuf; *lp; lp += *lp)
		if (getlineno(lp) >= lineno)
			break;
	return lp;
}

// Return free memory size
short getsize() {
	unsigned char* lp;

	for (lp = listbuf; *lp; lp += *lp);
	return listbuf + SIZE_LIST - lp - 1;
}

// Insert i-code to the list
// Preconditions to do *ibuf = len
void inslist() {
	unsigned char *insp;
	unsigned char *p1, *p2;
	short len;

	if (getsize() < *ibuf) {
		err = ERR_LBUFOF; // List buffer overflow
		return;
	}

	insp = getlp(getlineno(ibuf));

	if (getlineno(insp) == getlineno(ibuf)) {// line number agree
		p1 = insp;
		p2 = p1 + *p1;
		while (len = *p2) {
			while (len--)
				*p1++ = *p2++;
		}
		*p1 = 0;
	}

	// Case line number only
	if (*ibuf == 4)
		return;

	// Make space
	for (p1 = insp; *p1; p1 += *p1);
	len = p1 - insp + 1;
	p2 = p1 + *ibuf;
	while (len--)
		*p2-- = *p1--;

	// Insert
	len = *ibuf;
	p1 = insp;
	p2 = ibuf;
	while (len--) {
		*p1++ = *p2++;
	}
        // NOTE!! last char is 68('D') (I_EOL)

}

//Listing 1 line of i-code
void putlist(unsigned char* ip) {
	unsigned char i;

	while (*ip != I_EOL) {
		// Case keyword
		if (*ip < SIZE_KWTBL) {
			if (*ip==I_ELSE) {
				c_putch(' ');
			}
			c_puts(kwtbl[*ip]);
			if (!nospacea(*ip))
				c_putch(' ');
			if (*ip == I_REM) {
				ip++;
				i = *ip++;
				while (i--) {
					c_putch(*ip++);
				}
				return;
			}
			ip++;
		}
		else

		// Case numeric
		if (*ip == I_NUM) {
			ip++;
			putnum(*ip | *(ip + 1) << 8, 0);
			ip += 2;
			if (!nospaceb(*ip)) c_putch(' ');
		}
		else

		// Case binary
		if (*ip == I_BIN || *ip == I_0B) {
			if (*ip == I_BIN) {
				c_putch('`');
			} else {
				c_putch('0');
				c_putch('B');
			}
			ip++;
			i = *ip++;
			while (i--) {
				c_putch(*ip++);
			}
			if (!nospaceb(*ip)) c_putch(' ');
		}
		else

		// Case hex
		if (*ip == I_0X) {
			c_putch('0');
			c_putch('X');
			ip++;
			i = *ip++;
			while (i--) {
				c_putch(*ip++);
			}
			if (!nospaceb(*ip)) c_putch(' ');
		}
		else

		// Case color code
		if (*ip == I_COLCODE) {
			ip++;
			c_puts(color_names[*ip]);
			ip++;
			if (!nospacea(*ip)) c_putch(' ');
		}
		else

		// Case variable
		if (*ip == I_VAR) {
			ip++;
			c_putch(*ip++ + 'A');
			if (!nospaceb(*ip)) c_putch(' ');
		}
		else

		// Case string
		if (*ip == I_STR) {
			char c;

			c = '\"';
			ip++;
			for (i = *ip; i; i--)
				if (*(ip + i) == '\"') {
					c = '\'';
					break;
				}

			c_putch(c);
			i = *ip++;
			while (i--) {
				c_putch(*ip++);
			}
			c_putch(c);
			if (*ip == I_VAR)
				c_putch(' ');
		}

		// Nothing match, I think, such case is impossible
		else {
			err = ERR_SYS;
			return;
		}
	}
}

// Get argument in parenthesis
short getparam(){
	short value;

	if(*cip != I_OPEN){
		err = ERR_PAREN;
		return 0;
	}
	cip++;
	value = iexp();
	if(err) return 0;

	if(*cip != I_CLOSE){
		err = ERR_PAREN;
		return 0;
	}
	cip++;

	return value;
}

char* getFileName(){
	int len = *cip++;
	strncpy(fileName,cip,len);
	fileName[len]='\0';
	cip+=len;

	return fileName;
}


// Get value
short ivalue() {
	short value;
	short len;
	switch (*cip) {
	case I_BIN:
	case I_0B:
		cip++;
		len = *cip++;
		value=0;
		while (len--) {
			value = 2 * value + *cip++ - '0';
		}
		break;
	case I_0X:
		cip++;
		len = *cip++;
		value=0;
		while (len--) {
			short xcode=c_toupper(*cip++);
			if   (c_isdigit(xcode))  {xcode=xcode - '0';}
			else                     {xcode=xcode - 'A'+10;}
			value = 16 * value + xcode;
		}

		break;
	case I_NUM:
		cip++;
		value = *cip | *(cip + 1) << 8;
		cip += 2;
		break;
	case I_COLCODE:
		cip++;
		value = color_codes[*cip];
		cip ++;
		break;
	case I_PLUS:
		cip++;
		value = ivalue();
		break;
	case I_MINUS:
		cip++;
		value = 0 - ivalue();
		break;
	case I_VAR:
		cip++;
		value = var[*cip++];
		break;
	case I_OPEN:
		value = getparam();
		break;
	case I_ARRAY:
		cip++;
		value = getparam();
		if (err)
			break;
		if (value >= SIZE_ARRY) {
			err = ERR_SOR;
			break;
		}
		value = arr[value];
		break;
	case I_RND:
		cip++;
		value = getparam();
		if (err)
			break;
		value = getrnd(value);
		break;
	case I_ABS:
		cip++;
		value = getparam();
		if (err)
			break;
		if (value < 0)
			value *= -1;
		break;
	case I_IN:
		cip++;
		value = getparam();
		if (err)
			break;
		value = getin(gpioIn[value]);
		break;
	case I_ANA:
		cip++;
		value = getparam();
		if (err)
			break;
		value = getana(value);
		break;
	case I_INKEY:
		cip++;
		if(*cip != I_OPEN){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		if(*cip != I_CLOSE){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		value = getinkey();
		break;
	case I_YEAR:
		cip++;
		if(*cip != I_OPEN){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		if(*cip != I_CLOSE){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		value = getyear();
		break;
	case I_MONTH:
		cip++;
		if(*cip != I_OPEN){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		if(*cip != I_CLOSE){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		value = getmonth();
		break;
	case I_DAY:
		cip++;
		if(*cip != I_OPEN){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		if(*cip != I_CLOSE){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		value = getday();
		break;
	case I_HOUR:
		cip++;
		if(*cip != I_OPEN){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		if(*cip != I_CLOSE){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		value = gethour();
		break;
	case I_MINUTE:
		cip++;
		if(*cip != I_OPEN){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		if(*cip != I_CLOSE){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		value = getminute();
		break;
	case I_SECOND:
		cip++;
		if(*cip != I_OPEN){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		if(*cip != I_CLOSE){
			err = ERR_PAREN;
			return 0;
		}
		cip++;
		value = getsecond();
		break;
	case I_SIZE:
		cip++;
		if ((*cip != I_OPEN) || (*(cip + 1) != I_CLOSE)) {
			err = ERR_PAREN;
			break;
		}
		cip += 2;
		value = getsize();
		break;

	default:
		err = ERR_SYNTAX;
		break;
	}
	return value;
}

// multiply, divide or reminder calculation
short imul() {
	short value, tmp;

	value = ivalue();
	if (err)
		return -1;
	while (1)
		switch (*cip) {
		case I_MUL:
			cip++;
			tmp = ivalue();
			value *= tmp;
			break;
		case I_DIV:
			cip++;
			tmp = ivalue();
			if (tmp == 0) {
				err = ERR_DIVBY0;
				return -1;
			}
			value /= tmp;
			break;
		case I_REMINDER:
			cip++;
			tmp = ivalue();
			if (tmp == 0) {
				err = ERR_DIVBY0;
				return -1;
			}
			value %= tmp;
			break;
		default:
			return value;
		}
}

// add or subtract calculation
short iplus() {
	short value, tmp;

	value = imul();
	if (err)
		return -1;

	while (1)
		switch (*cip) {
		case I_PLUS:
			cip++;
			tmp = imul();
			value += tmp;
			break;
		case I_MINUS:
			cip++;
			tmp = imul();
			value -= tmp;
			break;
		default:
			return value;
		}
}

// The parser
short iexp() {
	short value, tmp;

	value = iplus();
	
	if (err)
		return -1;

	// conditional expression
	while (1)
		switch (*cip) {
		case I_EQ:
			cip++;
			tmp = iplus();
			value = (value == tmp);
			break;
		case I_SHARP:
			cip++;
			tmp = iplus();
			value = (value != tmp);
			break;
		case I_LT:
			cip++;
			tmp = iplus();
			value = (value < tmp);
			break;
		case I_LTE:
			cip++;
			tmp = iplus();
			value = (value <= tmp);
			break;
		case I_GT:
			cip++;
			tmp = iplus();
			value = (value > tmp);
			break;
		case I_GTE:
			cip++;
			tmp = iplus();
			value = (value >= tmp);
			break;
		default:
			return value;
		}
}

// LOCATE handler
void ilocate(){
	short xpos = iexp();
	if (*cip!=I_COMMA) {
		err = ERR_SYNTAX;
		return;
	}
	cip++;
	short ypos = iexp();
	basicLocate(xpos,ypos);
}

// PRINT handler
void iprint() {
	short value;
	short len;
	unsigned char i;

	len = 0;
	while (*cip != I_COLON && *cip != I_EOL && *cip != I_ELSE ) {
		switch (*cip) {
		case I_STR:
			cip++;
			i = *cip++;
			while (i--)
				c_putch(*cip++);
			break;
		case I_SHARP:
			cip++;
			len = iexp();
			if (err)
				return;
			break;
		default:
			value = iexp();
			if (err)
				return;
			putnum(value, len);
			break;
		}
		if (*cip == I_COMMA) {
			cip++;
			if (*cip == I_COLON || *cip == I_EOL || *cip == I_ELSE )
				return;
		}
		else {
			if (*cip != I_COLON && *cip != I_EOL && *cip != I_ELSE ) {
				err = ERR_SYNTAX;
				return;
			}
		}
	}
	newline();
}

// INPUT handler
void iinput() {
	short value;
	short index;
	unsigned char i;
	unsigned char prompt;
	while (1) {
		prompt = 1;

		if (*cip == I_STR) {
			cip++;
			i = *cip++;
			while (i--)
				c_putch(*cip++);
			prompt = 0;
		}

		switch (*cip) {
		case I_VAR:
			cip++;
			if (prompt) {
				c_putch(*cip + 'A');
				c_putch(':');
			}
			value = getnum();
			if (err)
				return;
			var[*cip++] = value;
			break;
		case I_ARRAY:
			cip++;
			index = getparam();
			if (err)
				return;
			if (index >= SIZE_ARRY) {
				err = ERR_SOR;
				return;
			}
			if (prompt) {
				c_puts("@(");
				putnum(index, 0);
				c_puts("):");
			}
			value = getnum();
			if (err)
				return;
			arr[index] = value;
			break;
		default:
			err = ERR_SYNTAX;
			return;
		}

		switch (*cip) {
		case I_COMMA:
			cip++;
			break;
		case I_COLON:
		case I_EOL:
		case I_ELSE:
			return;
		default:
			err = ERR_SYNTAX;
			return;
		}
	}
}

// OUT handler
void iout() {
	short val = iexp(); // get 0 or 1
        short valn = -1;
	if (*cip==I_COMMA) {
		cip++;
		valn=val;
		val=iexp();
	}


	if (valn>=0) {
		short val_bin = val==0? 0:1;
		short gpio_port=gpioOut[valn];
 
		setout(gpio_port,val_bin);
	} else {
		short p;
		// remind: outMode >=0  start  logical output at outMode
		//         outMode < 0  output in sequence of defined ports.
		short port= outMode>=0 ? outMode : 0;  
		
		for (p=0;p<16;p++) {
			short ov=val & 1;
			short gpio_port;
			if (outMode<0) {
				while(gpioOut[port]<0) {
					port++;
					if (port>=MAX_GPIO) break;
				}
				if (port>=MAX_GPIO) break;
			}
			
			gpio_port=gpioOut[port];
			setout(gpio_port,ov);
			val = val >> 1;
			port++;
			if (port>=MAX_GPIO) break;
		}
	}
}

// LED handler
void iled() {
	short ledval = iexp(); // get 0 or 1
// SHALL PROCESS LED VAL HERE!
}

// PWM handler
void ipwm() {
	short valn = iexp();
	short val;
	if (*cip==I_COMMA) {
		cip++;
		val=iexp();
	} else {
		err = ERR_SYNTAX;
		return;
	}
	short gpio_port=gpioPwm[valn];
	setpwm(gpio_port,val);
}


// SERVO handler
void iservo() {
	short valn = iexp();
	short val;
	if (*cip==I_COMMA) {
		cip++;
		val=iexp();
	} else {
		err = ERR_SYNTAX;
		return;
	}
	short gpio_port=gpioServo_[valn];
	setservo(gpio_port,val);
}


// COLOR handler
void icolor() {
	short val1 = iexp();
	if (val1<-1 || val1>255) {
		err = ERR_SYNTAX;
		return;
	}
        short val2 = -1;
	if (*cip==I_COMMA) {
		cip++;
		val2=iexp();
		if (val2<0 || val2>255) {
			err = ERR_SYNTAX;
			return;
		}
	}
	basicSetColor(val1,val2);
}

// SGR handler
void isgr() { // https://en.wikipedia.org/wiki/ANSI_escape_code
	short val = iexp();
	if (val<0 || val>256) {
		err = ERR_SYNTAX;
		return;
	}
		
	if (val!=38 && val!=48) { // 38 and 48: use color function
		basicSetSGR(val);
	}
}

// RESIZE handler
void iresize() {
	short valw = iexp();
	short valh;
	if (*cip==I_COMMA) {
		cip++;
		valh=iexp();
	} else {
		err = ERR_SYNTAX;
		return;
	}
	basicScreenResize(valw,valh);
}


// Variable assignment handler
void ivar() {
	short value;
	short index;

	index = *cip++;
	if (*cip != I_EQ) {
		err = ERR_VWOEQ;
		return;
	}
	cip++;
	value = iexp();
	
	if (err)
		return;

	var[index] = value;
}

// Array assignment handler
void iarray() {
	short value;
	short index;

	index = getparam();
	if (err)
		return;

	if (index >= SIZE_ARRY) {
		err = ERR_SOR;
		return;
	}

	if (*cip != I_EQ) {
		err = ERR_VWOEQ;
		return;
	}
	cip++;

	value = iexp();
	if (err)
		return;

	arr[index] = value;
}

// LET handler
void ilet() {
	switch (*cip) {
	case I_VAR:
		cip++;
		ivar(); // Variable assignment
		break;
	case I_ARRAY:
		cip++;
		iarray(); // Array assignment
		break;
	default:
		err = ERR_LETWOV;
		break;
	}
}

// Execute a series of i-code
unsigned char* iexe() {
	short lineno; //line number
	unsigned char* lp; //temporary line pointer
	short index, vto, vstep; // FOR-NEXT items
	short condition; //IF condition
	while (*cip != I_EOL) {
		if (c_check_esc()) {// check keyin
			err = ERR_ESC;
			c_getch();
			return NULL;
		}         
		switch (*cip) {
		case I_GOTO:
			cip++;
			lineno = iexp(); // get line number
			if (err)
				break;
			lp = getlp(lineno); // search line
			if (lineno != getlineno(lp)) { // if not found
				err = ERR_ULN;
				break;
			}

			clp = lp; // update line pointer
			cip = clp + 3; // update i-code pointer
			break;

		case I_GOSUB:
			cip++;
			lineno = iexp(); // get line number
			if (err)
				break;
			lp = getlp(lineno); // search line
			if (lineno != getlineno(lp)) { // if not found
				err = ERR_ULN;
				break;
			}

			// push pointers
			if (gstki >= SIZE_GSTK - 3) { // stack overflow ?
				err = ERR_GSTKOF;
				break;
			}
			gstk[gstki++] = clp; // push line pointer
			gstk[gstki++] = cip; // push i-code pointer
			gstk[gstki++] = (unsigned char*)lstki; //FOR stack index

			clp = lp; // update line pointer
			cip = clp + 3; // update i-code pointer
			break;

		case I_RETURN:
			if (gstki < 3) { // stack empty ?
				err = ERR_GSTKUF;
				break;
			}
			lstki = (int)gstk[--gstki]; //FOR stack index
			cip = gstk[--gstki]; // pop line pointer
			clp = gstk[--gstki]; // pop i-code pointer
			break;

		case I_FOR:
			cip++;

			if (*cip++ != I_VAR) { // no variable
				err = ERR_FORWOV;
				break;
			}

			index = *cip; // get variable index
			ivar(); // var = value
			if (err)
				break;

			if (*cip == I_TO) {
				cip++;
				vto = iexp(); // get TO value
			}
			else {
				err = ERR_FORWOTO;
				break;
			}

			if (*cip == I_STEP) {
				cip++;
				vstep = iexp(); // get STEP value
			}
			else
				vstep = 1; // default STEP value

						   // overflow check
			if (((vstep < 0) && (-32767 - vstep > vto)) ||
				((vstep > 0) && (32767 - vstep < vto))) {
				err = ERR_VOF;
				break;
			}

			// push pointers
			if (lstki >= SIZE_LSTK - 5) { // stack overflow ?
				err = ERR_LSTKOF;
				break;
			}
			lstk[lstki++] = clp; // push line pointer
			lstk[lstki++] = cip; // push i-code pointer
								 //Special thanks hardyboy
			lstk[lstki++] = (unsigned char*)(uintptr_t)vto; // push TO value
			lstk[lstki++] = (unsigned char*)(uintptr_t)vstep; // push STEP value
			lstk[lstki++] = (unsigned char*)(uintptr_t)index; // push variable index
			break;

		case I_NEXT:
			cip++;

			if (lstki < 5) { // stack empty ?
				err = ERR_LSTKUF;
				break;
			}

			index = (short)(uintptr_t)lstk[lstki - 1]; // read variable index
			if (*cip == I_EOL) { // no variable ... => no variable check
			} else {
				if (*cip++ != I_VAR) { // no variable
					err = ERR_NEXTWOV;
					break;
				}
				if (*cip++ != index) { // not equal index
					err = ERR_NEXTUM;
					break;
				}      
			}

			vstep = (short)(uintptr_t)lstk[lstki - 2]; // read STEP value
			var[index] += vstep; // update loop counter
			vto = (short)(uintptr_t)lstk[lstki - 3]; // read TO value

													 // loop end
			if (((vstep < 0) && (var[index] < vto)) ||
				((vstep > 0) && (var[index] > vto))) {
				lstki -= 5; // resume stack
				break;
			}

			// loop continue
			cip = lstk[lstki - 4]; // read line pointer
			clp = lstk[lstki - 5]; // read i-code pointer
			break;

		case I_IF:
			cip++;
			condition = iexp(); // get condition
			if (err) {
				err = ERR_IFWOC;
				break;
			}
			if (condition) // if true continue
				break;

			// check else
			while (*cip != I_EOL) {
				cip++; // seek end of line
				if (*cip==I_ELSE) {
					cip++;
					break;
				}
			}
			break;
			
		case I_ELSE:
			// If reached ELSE, shall do nothing. Same as REM

		case I_OK:
		case I_YOU:
		case I_SYNTAX:
		case I_REM:
			// Seek pointer to I_EOL
			// No problem even if it points not realy end of line
			while (*cip != I_EOL)
				cip++; // seek end of line
			break;

		case I_STOP:
		case I_END:
			while (*clp)
				clp += *clp; // seek end
			return clp;

		case I_VAR:
			cip++;
			ivar();
			break;
		case I_ARRAY:
			cip++;
			iarray();
			break;
		case I_LET:
			cip++;
			ilet();
			break;
		case I_LOCATE:
		case I_LC:
			cip++;
			ilocate();
			break;
		case I_PRINT:
		case I_QUESTION:
			cip++;
			iprint();
			break;
		case I_WAIT:
			cip++;
			waittime_cs = iexp();
			useconds_t s_time=10000*(int)waittime_cs;
//			int r=usleep(s_time);  // usleep is outadated

			struct timespec ts_sleep;
			ts_sleep.tv_sec = s_time/1000000;
			ts_sleep.tv_nsec = 1000*(s_time%1000000);
			int r=  nanosleep(&ts_sleep, NULL);
			break;
		case I_INPUT:
			cip++;
			iinput();
			break;
		case I_OUT:
			cip++;
			iout();
			break;
		case I_LED:
			cip++;
			iled();
			break;
		case I_PWM:
			cip++;
			ipwm();
			break;
		case I_SERVO:
			cip++;
			iservo();
			break;
		case I_COLOR:
			cip++;
			icolor();
			break;
		case I_SGR:
			cip++;
			isgr();
			break;
		case I_RESIZE:
			cip++;
			iresize();
			break;
		case I_COLON:
			cip++;
			break;
		case I_CLS:
			cip++;
			basicCls();
			break;
		case I_LIST:
		case I_SAVE:
		case I_LOAD:
		case I_NEW:
		case I_RUN:
			err = ERR_COM;
			break;

		default:
			err = ERR_SYNTAX;
			break;
		}

		if (err)
			return NULL;
	}
	return clp + *clp;
}

// RUN command handler
void irun() {
	unsigned char* lp;

	gstki = 0;
	lstki = 0;
	clp = listbuf;

	while (*clp) {
		cip = clp + 3;
		lp = iexe();
		if (err)
			return;
		clp = lp;
	}
}

// LIST command handler
void ilist() {
	short lineno;

	lineno = (*cip == I_NUM) ? getlineno(cip) : 0;

	for (clp = listbuf;
	*clp && (getlineno(clp) < lineno);
		clp += *clp);
		while (*clp) {
			putnum(getlineno(clp), 0);
			c_putch(' ');
			putlist(clp + 3);
			if (err)
				break;
			newline();
			clp += *clp;
		}
}

//NEW command handler
void inew(void) {
	unsigned char i;

	for (i = 0; i < 26; i++)
		var[i] = 0;
	for (i = 0; i < SIZE_ARRY; i++)
		arr[i] = 0;
	gstki = 0;
	lstki = 0;
	*listbuf = 0;
	clp = listbuf;
}

//Command precessor
void icom() {
	cip = ibuf;
	b_file=NULL;
	
	switch (*cip) {
	case I_NEW:
		cip++;
		if (*cip == I_EOL)
			inew();
		else
			err = ERR_SYNTAX;
		break;
	case I_SAVE:
		cip++;
		if (*cip == I_STR) {
			cip++;
			char *fileName=getFileName();
			b_file=fopen(fileName,"w");
			if (b_file==NULL) {
				err = ERR_SYNTAX;
				break;
			}
		} else {
			err = ERR_SYNTAX;
			break;
		}
		// contents will be writen with list below
	case I_LIST:
		if (b_file==NULL) { // not save mode
			cip++;
		}
		if (*cip == I_EOL || *(cip + 3) == I_EOL) {
			ilist();
		} else {
			err = ERR_SYNTAX;
		}
		if (b_file) {
			fclose(b_file);
			b_file=NULL;
		}
		break;
	case I_LOAD:
		cip++;
		if (*cip == I_STR) {
			cip++;
			char *fileName=getFileName();
			b_file=fopen(fileName,"r");
			if (b_file==NULL) {
				err = ERR_LOAD;
				break;
			}
		} else {
			err = ERR_SYNTAX;
			break;
		}
		inew();
		char line[512];
		char p;
		while(p=fgets(line,2048,b_file)){
			line[strlen(line)-1]='\0'; // delete new line
			execBasic(line);
		}
		fclose(b_file);
		b_file=NULL;
		break;
	case I_RUN:
		cip++;
		irun();
		break;
	default:
		iexe();
		break;
	}

}

// Print OK or error message
void error() {
	if (err) {
		if (cip >= listbuf && cip < listbuf + SIZE_LIST && *clp)
		{
			newline();
			c_puts("LINE:");
			putnum(getlineno(clp), 0);
			c_putch(' ');
			putlist(clp + 3);
		}
		else
		{
			newline();
			c_puts("YOU TYPE: ");
			c_puts(lbuf);
		}
		newline();
	}
	c_puts(errmsg[err]);
	newline();
	err = 0;
}

void bufferError(){
  err=ERR_IBUFOF;
  error();
}

void initBasic(char *cfgFile){
  
	if (cfgFile) {
		initGpio();
		configGpio(cfgFile);
		if (adcModel>=0) { // with ADC chip
			initAdc();
		}
	} else {
		srand((unsigned int)time(0)); // for RND function
		inew();
 
		c_puts("Tanba Labs BASIC 0.1 ");
		c_puts(getHWName());
		c_puts(" https://gitlab.com/blsky/tanbabasic");
		newline();
		error(); // Print OK, and Clear error flag
	}
	basicSetColor(231,21);
}

void terminateBasic(){
	terminateGpio();	
	if (adcModel>=0) { // with ADC chip
		terminateAdc();
	}
}

void execBasic(char *cmd){
	unsigned char len;
	//c_gets(cmd); // Input 1 line
	c_convert_cmd(cmd);
	len = toktoi(); // Convert token to i-code
	if(err){ // Error
		error();
		return; // Do nothing
	}
	
	if(*ibuf == I_SYSTEM){
		return;
	}

	newline();
	if(*ibuf == I_NUM){ // Case the top includes line number
		*ibuf =	     len; // Change I_NUM to byte length
		inslist(); // Insert list
		if (err) // Error
			error();  // Print error message
		return;
	}

	icom(); // Execute direct
	error(); // Print OK, and Clear error flag
 
}

