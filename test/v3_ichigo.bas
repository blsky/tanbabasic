1 REM use: ./tanbabasic test/rpiv3_bcm_ichigo.cfg
2 REM      ./tanbabasic test/rpiv3_board_ichigo.cfg
3 REM tested with buttons and LEDs
10 A=IN(1)
11 B=IN(2)
12 C=IN(3)
13 D=IN(4)
14 E=IN(9)
20 ? A,B,C,D,E
21 OUT 1,A
22 OUT 2,B
23 OUT 3,C
24 OUT 4,D
25 OUT 5,D
26 OUT 6,E
27 OUT 7,E
30 WAIT 50
40 GOTO 10
