1 REM use: ./tanbabasic test/rpiv1_bcm_in_out.cfg
2 REM tested with buttons and LEDs
10 A=IN(17)
11 B=IN(18)
12 C=IN(21)
20 ? A,B,C
21 OUT 22,A
22 OUT 23,B
23 OUT 24,C
24 OUT 25,C
30 WAIT 50
40 GOTO 10
