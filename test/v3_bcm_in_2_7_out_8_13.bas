1 REM use: ./tanbabasic test/rpiv3_bcm_in_out.cfg
2 REM tested with buttons and LEDs
10 A=IN(2)
11 B=IN(3)
12 C=IN(4)
13 D=IN(5)
14 E=IN(6)
15 F=IN(7)
20 ? A,B,C,D,E,F
21 OUT 8,A
22 OUT 9,B
23 OUT 10,C
24 OUT 11,D
25 OUT 12,E
26 OUT 13,F
30 WAIT 50
40 GOTO 10
