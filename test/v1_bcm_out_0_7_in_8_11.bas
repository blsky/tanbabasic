1 REM use: ./tanbabasic test/rpiv1_bcm_in_out.cfg
2 REM tested with buttons and LEDs
10 A=IN(8)
11 B=IN(9)
12 C=IN(10)
13 D=IN(11)
20 ? A,B,C,D
21 OUT 0,A
22 OUT 1,B
23 OUT 4,C
24 OUT 7,D
30 WAIT 50
40 GOTO 10
