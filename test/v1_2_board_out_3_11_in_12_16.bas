1 REM use: ./tanbabasic test/rpiv1_2_board_out_in.cfg
2 REM tested with buttons and LEDs
10 A=IN(12)
11 B=IN(13)
12 C=IN(15)
13 D=IN(16)
20 ? A,B,C,D
21 OUT 3,A
22 OUT 5,B
23 OUT 7,C
24 OUT 11,D
30 WAIT 50
40 GOTO 10
