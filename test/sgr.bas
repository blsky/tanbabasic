10 CLS 
20 ? "SGR test. May not work on some terminals. Each mode duration is 2 sec"
30 ? "============"
40 SGR 4
50 ? "SGR 4   underline"
60 WAIT 200
70 SGR 7
80 ? "SGR 7   reverse video"
90 WAIT 200
100 SGR 31
110 ? "SGR 31   foreground color red"
120 WAIT 200
130 SGR 44
140 ? "SGR 44   background color blue"
150 WAIT 200
160 SGR 91
170 ? "SGR 91   foreground color bright red"
180 WAIT 200
190 SGR 105
200 ? "SGR 105   background color bright magenta"
210 WAIT 200
