1 REM use: ./tanbabasic test/rpiv3_board_in_out.cfg
2 REM tested with buttons and LEDs
10 A=IN(3)
11 B=IN(5)
12 C=IN(7)
13 D=IN(11)
14 E=IN(12)
15 F=IN(13)
20 ? A,B,C,D,E,F
21 OUT 15,A
22 OUT 16,B
23 OUT 18,C
24 OUT 19,D
25 OUT 21,E
26 OUT 22,F
30 WAIT 50
40 GOTO 10
