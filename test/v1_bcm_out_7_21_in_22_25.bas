1 REM use: ./tanbabasic test/rpiv1_bcm_in_out.cfg
2 REM tested with buttons and LEDs
10 A=IN(22)
11 B=IN(23)
12 C=IN(24)
13 D=IN(25)
20 ? A,B,C
21 OUT 7,A
22 OUT 17,B
23 OUT 18,C
24 OUT 21,D
30 WAIT 50
40 GOTO 10
