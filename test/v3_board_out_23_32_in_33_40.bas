1 REM use: ./tanbabasic test/rpiv3_board_out_in.cfg
2 REM tested with buttons and LEDs
10 A=IN(33)
11 B=IN(35)
12 C=IN(36)
13 D=IN(37)
14 E=IN(38)
15 F=IN(40)
20 ? A,B,C,D,E,F
21 OUT 23,A
22 OUT 24,B
23 OUT 26,C
24 OUT 29,D
25 OUT 31,E
26 OUT 32,F
30 WAIT 50
40 GOTO 10
