1 REM use: ./tanbabasic test/rpiv3_board_out_in.cfg
2 REM tested with buttons and LEDs
10 A=IN(15)
11 B=IN(16)
12 C=IN(18)
13 D=IN(19)
14 E=IN(21)
15 F=IN(22)
20 ? A,B,C,D,E,F
21 OUT 3,A
22 OUT 5,B
23 OUT 7,C
24 OUT 11,D
25 OUT 12,E
26 OUT 13,F
30 WAIT 50
40 GOTO 10
