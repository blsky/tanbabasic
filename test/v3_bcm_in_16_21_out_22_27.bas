1 REM use: ./tanbabasic test/rpiv3_bcm_in_out.cfg
2 REM tested with buttons and LEDs
10 A=IN(16)
11 B=IN(17)
12 C=IN(18)
13 D=IN(19)
14 E=IN(20)
15 F=IN(21)
20 ? A,B,C,D,E,F
21 OUT 22,A
22 OUT 23,B
23 OUT 24,C
24 OUT 25,D
25 OUT 26,E
26 OUT 27,F
30 WAIT 50
40 GOTO 10
