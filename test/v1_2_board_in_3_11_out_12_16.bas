1 REM use: ./tanbabasic test/rpiv1_2_board_in_out.cfg
2 REM tested with buttons and LEDs
10 A=IN(3)
11 B=IN(5)
12 C=IN(7)
13 D=IN(11)
20 ? A,B,C,D
21 OUT 12,A
22 OUT 13,B
23 OUT 15,C
24 OUT 16,D
30 WAIT 50
40 GOTO 10
