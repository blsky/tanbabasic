1 REM use: ./tanbabasic test/rpiv3_bcm_out_in.cfg
2 REM tested with buttons and LEDs
10 A=IN(22)
11 B=IN(23)
12 C=IN(24)
13 D=IN(25)
14 E=IN(26)
15 F=IN(27)
20 ? A,B,C,D,E,F
21 OUT 16,A
22 OUT 17,B
23 OUT 18,C
24 OUT 19,D
25 OUT 20,E
26 OUT 21,F
30 WAIT 50
40 GOTO 10
