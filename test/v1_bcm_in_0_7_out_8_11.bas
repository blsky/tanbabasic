1 REM use: ./tanbabasic test/rpiv1_bcm_in_out.cfg
2 REM tested with buttons and LEDs
10 A=IN(0)
11 B=IN(1)
12 C=IN(4)
13 D=IN(7)
20 ? A,B,C,D
21 OUT 8,A
22 OUT 9,B
23 OUT 10,C
24 OUT 11,D
30 WAIT 50
40 GOTO 10
