1 REM use: ./tanbabasic test/rpiv1_2_board_out_in.cfg
2 REM tested with buttons and LEDs
10 A=IN(22)
11 B=IN(23)
12 C=IN(24)
13 D=IN(26)
20 ? A,B,C,D
21 OUT 11,A
22 OUT 18,B
23 OUT 19,C
24 OUT 21,D
30 WAIT 50
40 GOTO 10
