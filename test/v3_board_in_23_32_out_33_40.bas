1 REM use: ./tanbabasic test/rpiv3_board_in_out.cfg
2 REM tested with buttons and LEDs
10 A=IN(23)
11 B=IN(24)
12 C=IN(26)
13 D=IN(29)
14 E=IN(31)
15 F=IN(32)
20 ? A,B,C,D,E,F
21 OUT 33,A
22 OUT 35,B
23 OUT 36,C
24 OUT 37,D
25 OUT 38,E
26 OUT 40,F
30 WAIT 50
40 GOTO 10
