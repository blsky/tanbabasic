1 REM use: ./tanbabasic test/rpiv3_bcm_out_mode_default.cfg    => set OUT_MODE to  0
2 REM      ./tanbabasic test/rpiv3_bcm_out_mode_0.cfg          => set OUT_MODE to  0
3 REM      ./tanbabasic test/rpiv3_bcm_out_mode_2.cfg          => set OUT_MODE to  2
4 REM      ./tanbabasic test/rpiv3_bcm_out_mode_defined.cfg    => set OUT_MODE to -1
6 REM all the above sets logical ports 2,5,6 only
5 REM  tested with 3 LEDs.
10  REM  OUT_MODE     -1     0     2
20  REM  LED         652   652   652
30  ?  "0B00000001 MODE/LED(652)   -1/001   0/000   2/001"
31  OUT 0B00000001
32  WAIT 200
40  ?  "0B00000010 MODE/LED(652)   -1/010   0/000   2/000"
41  OUT 0B00000010
42  WAIT 200
50  ?  "0B00000100 MODE/LED(652)   -1/100   0/001   2/000"
51  OUT 0B00000100
52  WAIT 200
60  ?  "0B00001000 MODE/LED(652)   -1/000   0/000   2/010"
61  OUT 0B00001000
62  WAIT 200
70  ?  "0B00010000 MODE/LED(652)   -1/000   0/000   2/100"
71  OUT 0B00010000
72  WAIT 200
80  ?  "0B00100000 MODE/LED(652)   -1/000   0/010   2/000"
81  OUT 0B00100000
82  WAIT 200
90  ?  "0B01000000 MODE/LED(652)   -1/000   0/100   2/000"
91  OUT 0B01000000
92  WAIT 200
100 ?  "0B10000000 MODE/LED(652)   -1/000   0/000   2/000"
101 OUT 0B10000000
102 WAIT 200
110 ?  "0B00000111 MODE/LED(652)   -1/111   0/001   2/001"
111 OUT 0B00000111
112 REM              
112 WAIT 200
120 ?  "0B01111000 MODE/LED(652)   -1/000   0/110   2/110"
121 OUT 0B01111000
122 WAIT 200
