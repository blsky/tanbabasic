1 REM use: ./tanbabasic test/rpiv3_bcm_out_in.cfg
2 REM tested with buttons and LEDs
10 A=IN(8)
11 B=IN(9)
12 C=IN(10)
13 D=IN(11)
14 E=IN(12)
15 F=IN(13)
20 ? A,B,C,D,E,F
21 OUT 2,A
22 OUT 3,B
23 OUT 4,C
24 OUT 5,D
25 OUT 6,E
26 OUT 7,F
30 WAIT 50
40 GOTO 10
