## TANBA BASIC

[日本語](https://tanbalabs.hatenablog.com/entry/2020/08/03/221223)

I am implementing this BASIC interpreter for teaching purposes.
Currently, it works for most of my needs (my requirements below), including access to Raspberry Pi's GPIOs.
However, since I don't have much time, I have done only limited tests. Max 20 lines of code, because my student is still slow on typing.

* My RPI's are OK after the tests, but as on the license, this program has no warranty.

## Why BASIC

The first programming language I learned is BASIC on a Sinclair clone.
It was the best way to learn the basics of a programming language.

Today, there are endless discussions of that would be the best first programming language to teach.
I have thought and read many opinions, and reached the conclusion that it is BASIC (I will not enter a discussion on this here), but could not imagine how I could do it with current computers.

One day, I learned about [Ichigo-jam computer](https://ichigojam.net/index-en.html). '*Ichigo*' means strawberry in Japanese, '*Ichi*' is one, and '*go*' is five, i.e., a strawberry jam computer for 1500 JPY (app. US$ 15). This is a small board with black/white BASIC interpreter and GPIO.

I got my sample and started playing with LED's, sensors, motors and solders with my very young student.

Another day, came the idea to make a remote controlled simple toy car, or make and debug a controller without a monitor.
This required the Ichigo-jam to be wireless. There's a version of Ichigo-jam for Raspberry-pi, but this is an ISO image that does not support RPI's WiFi hardware.

So, I looked around for a BASIC interpreter for Linux. I would add GPIO, install on an RPI zero with WiFi running on battery, remotely login on it, and run the interpreter! However, although I could find some, none could satisfying my requirements. Some had no IDE, others were too heavy or too complex to allow me to add GPIO interface.

Conclusion: DIY.

![](pic/tanba_terminal.png)

## My system requirements:

* Lightweight and simple (few lines of codes)
* Simple IDE as on Sinclair or MSX
* WiFi support
* GPIO support
* Configuration file for GPIO

Optional requirements:

* Color
* Line wrap on IDE
* Load/Save and others functions
* Can use both upper/lower case letters. However all commands are registered as upper case.
* Use ":" as command separator
* Can enter bin and hex numbers
* ADC input
* ELSE and END for IF (Very limited ELSE implemented. See below)

## Implementation details:

This software is divided in three parts, and I was able to implement because I could find initial source code to add my requirements (Many thanks to the authors!):

* Basic Interpreter
  Based on [TOYOSHIKI's Palo ALTO TinyBASIC implementation](https://github.com/vintagechips/ttbasic_lin).
* IDE
  Based on [Antirez's simple Kilo editor](https://github.com/antirez/kilo).
* GPIO IF
  I am using [pigpio for RPI](http://abyz.me.uk/rpi/pigpio/index.html).

![](pic/tanba_demo.png)

## Compile and run without GPIO

Compile without gpio (GPIO commands have no effect)

    make 

Run:

    ./tanbabasic 

## Compile and run with GPIO support for RPI

You will have to install pigpio

    (sudo apt-get install pigpio)
    
    make rpi

run:  (config file format below)

    sudo ./tanbabasic <config file>

## Tanba BASIC IDE commands:

CTRL + S   Save IDE editor data (This is NOT BASIC save command)

ESC        Abort program execution

CTRL + Q   Quit Tanba BASIC. If running a program, first abort with ESC.

<u>Issues:</u>
Some times, ESC key stops working (Cannot stop execution).
I could not reproduce this bug yet. The only way to terminate is to kill the process. Hope I could fix it.

## Tanba BASIC commands:

In addition to [TinyBasic commands](http://www.jk-quantized.com/experiments/8080Emulator/TinyBASIC-2.0.pdf), this supports the followings, inspired on Ichigo-jam and MSX.

| <div style="width:200px">Cmd</div> | Description                                                                                                                                                                                                                                                                              |
| ---------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| INKEY()                            | Get code of a pressed key. Special keys:<br> &nbsp;  &nbsp; 0 : No Key<br> &nbsp;  &nbsp; 9 : TAB<br> &nbsp;  &nbsp; 13: RETURN<br> &nbsp;  &nbsp; 28: RIGHT<br> &nbsp;  &nbsp; 29: LEFT<br> &nbsp;  &nbsp; 30: UP<br> &nbsp;  &nbsp; 31: DOWN<br>                                       |
| ELSE for IF in a single line       | Original TinyBasic has IF only, and everything (IF and commands after the condition) shall be in a single line. It is now possible to use ELSE. It is still limited to a single line, and the line shall have only one IF (If there are multiple IF with ELSE, the behavior is unknown). |
| CLS                                | Clear screen                                                                                                                                                                                                                                                                             |
| COLOR N1,N2                        | Set color. N1: foreground color, N2:(optional)  background color.<br> Use COLOR <mark>-1</mark>,N2 to set only background.<br> N1 and N2 are 0-255 numbers, or a color code (below). <br>This function is overridden by the following SGR: 30-37,39,90-97,40-47,49,100-107               |
| SGR N                              | [Set terminal SGR](https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_parameters).<br> Has no effect for 38 and 48 (Set by COLOR command).<br> TanbaBASIC special code: HL or HIGHLIGHT will switch to basic highlight mode.<br> Any other code will turn off highlight mode.            |
| LOCATE X,Y                         | Move cursor to position X,Y                                                                                                                                                                                                                                                              |
| LC X,Y                             | Same as LOCATE                                                                                                                                                                                                                                                                           |
| END                                | Same as original TinyBASIc STOP                                                                                                                                                                                                                                                          |
| LOAD "STR"                         | Load a program from file specified by STR (Linux format) <br>(<mark>NOTE</mark>: Some times, it does not load. I need to debug)                                                                                                                                                          |
| SAVE "STR"                         | Save current program into a file specified by STR                                                                                                                                                                                                                                        |
| RESIZE W,H                         | Resize terminal screen to W,H <br>(<mark>NOTE</mark>: This does not work on some terminals)                                                                                                                                                                                              |
| WAIT TIME                          | Wait TIME in centiseconds (wsl1 will not wait due to [this bug](https://github.com/microsoft/WSL/issues/4898))                                                                                                                                                                           |
| ?                                  | ? is a short format for PRINT                                                                                                                                           |
| %                                  | reminder operation                                                                                                                                                      |
| NEXT                               | Original TinyBASIC requires variable. Now, it does not need anymore.                                                                                                                                                                                                                     |

RPI GPIO commands. Port numbers are set by the config file (below)

You will have to implement additional circuits for IO.

| <div style="width:200px">Cmd</div> | Description                                                                                                                                                               |
| ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| IN(N)                              | Get input from logical port N                                                                                                                                             |
| OUT N1,N2                          | Output N2 to logical port N1. If N2>0, will set 1 to port N1.                                                                                                             |
| OUT N                              | Each bit of N(16 bits) goes to a logical port only. Logical port order is selected by the OUT_MODE in the configuration file. See below.                                  |
| ANA(N)                             | Get input from analog channel N. Needs additional ADC converter. Output is 0 is channel is out of range, or the ADC is not configured in the GPIO config file. See below. |

If I have time and hardware(Chip and motor), I intend to finish implementation and test of motor commands PWM and SERVO.

## Number format

Number can be binary, decimal or hexadecimal.
Binary numbers can be represented with initial ` or 0B.
Hexadecimals can be represented with initial 0X.
Example: all of the following corresponds to decimal 10:

    `1010
    0B1010
    0XA

Examples:

    A=0XA
    ? 0XA
    OUT 0XA

## Supported RPI's

I own the following, so the tests were limited to these hardwares:

* Original Model B
* Pi3B
* Pi zero

As [this link](http://abyz.me.uk/rpi/pigpio/index.html), there are 3 types of GPIOs.
Since my hardwares cover type 1 and 3, the software will not work for type 2.
If you want to use this software on type 2, you can comment some lines on rpi.c (if (type==2) part), and try on your own risk.

## GPIO config file format for RPI

The config file sets port number as well as modes.
There are two port numbering on RPI: [BOARD (pin) and BCM (GPIO numbers)](  https://raspi.tv/2013/rpi-gpio-basics-4-setting-up-rpi-gpio-numbering-systems-and-inputs)
Tanba BASIC allows the setting of logical port numbers as follows:

1) Use the same numbering as BOARD
2) Use the same numbering as BCM
3) Map different logical port numbers to BOARD or BCM
   (Ex: Use [same port numbers as Ichigo-jam](s://ichigojam.github.io/RPi/index.html) mapped into RPI)

I have also made a printable version of IchigoJam pins on RPI GPIO ([pdf](https://drive.google.com/open?id=1fdgJGYMk70KnY_RBdkmS0P8w5KdBUcST),[ods](https://drive.google.com/open?id=1d9wnPnPT0I5q5z9m9sXbQgxBzSIlKIOI)).
![](pic/ichigo_zero.jpg)

Each port can be set to input (pullup/down)  or output, with possible extension for PWM/SERVO output in future.

File format:

| <div style="width:100px">Item</div> | Description                                                                                                                                            |
| ----------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
| #                                   | Everything after '#' in a line are comments                                                                                                            |
| BOARD or BCM                        | First valid line shall be BOARD or BCM to define RPI port numbering mode to use.                                                                       |
| OUT_MODE M                          | Optional on the first line after BOARD or BCM. Used by OUT N command. See below.                                                                       |
| ADC_MAIN ADC_TYPE                   | Optional after the first line. ADC_TYPE is the supported ADC chip. See below.                                                                          |
| P,OUT                               | Set GPIO port P (BOARD of BCM) to output. Logical port number is also P.                                                                               |
| P,OUT<mark>N</mark>                 | Set GPIO port P (BOARD of BCM) to output. Logical port number is <mark>N</mark>. NOTE: There's no space between OUT and <mark>N</mark>                 |
| P,IN,MODE                           | Set GPIO port P (BOARD of BCM) to input. Logical port number is also P. MODE is optional and can be PULLOFF(clear), PULLUP or PULLDOWN                 |
| P,IN<mark>N</mark>,MODE             | Set GPIO port P (BOARD of BCM) to input. Logical port number is <mark>N</mark>. MODE is as above. NOTE: There's no space between IN and <mark>N</mark> |

The first valid line can be:

    BOARD   
    BCM
    BOARD OUT_MODE M
    BCM OUT_MODE M

M in OUT_MODE defines how each bit of N in OUT N goes to logical output port:

<u>M>=0</u> : logical port number starts at M. Will not output for not defined port numbers.

<u>M< 0</u> : output in order of defined logical ports only.

Default M is 0.

Example (config files and program below):
![](pic/out_mode.png)

## Supported ADC

As on the GPIO config file, the ADC chip can be configured as follows:

    ADC_MAIN ADC_TYPE

ADC_MAIN means that only the Main SPI can be used(Aux SPI not implemented)
http://abyz.me.uk/rpi/pigpio/cif.html#spiOpen

I have implemented code for the following ADC_TYPE.
I have verified the transmitted data as on the figure below for all types, but since I own only MCP3202, the complete testing is limited to this hardware.

    MCP3002
    MCP3004
    MCP3008
    MCP3202 (Complete test only with this hardware)
    MCP3204
    MCP3208

Wiring the ADC with Raspberry PI Main SPI, and how data is exchanged is as follows:
MCP operates on Single-Ended mode (SGL/DIFF set to 1). Also, if required, MSBF is set to 0. Check the details on the MCP Datasheet on the Net.
And as on the circuit diagram the chip is enabled by CE0 only.

To get ADC data, use ANA(N) command as described above (N is channel number).

![](pic/adc_mcp.png)

## Examples

Following are examples of some commands. Use LOAD command to load the files followed by RUN.

| <div style="width:100px">Program</div> | Description                                    |
| -------------------------------------- | ---------------------------------------------- |
| test/lc.bas                            | LC example                                     |
| test/inkey.bas                         | INKEY example. Press ESC to stop execution     |
| test/resize.bas                        | RESIZE example. May not work on some terminals |
| test/color.bas                         | COLOR example                                  |
| test/sgr.bas                           | SGR example                                    |
| test/bin_hex.bas                       | Binary and Hex numbers example                 |

## GPIO examples

I've tested GPIO with button for input and LEDs/Motors for output.

If you are a beginner, please, [read this](https://grantwinney.com/using-pullup-and-pulldown-resistors-on-the-raspberry-pi/) carefully for input circuit with button, to avoid the circuit being bounced between 0 and 1.
I have tested with both PULLUP and PULLDOWN resistors.

As above, you can set the logical port numbers to
a) Same number as pin number
b) Same number as GPIO number
c) Any other number

Below are examples for the 3 above, setting the SAME PHYSICAL pin to OUT, but different logical numbers.
![](pic/out_example.png)

I tested with PULLUP/DOWN buttons as input and LEDs as output.

Note that I have not tested UART GPIO14 and GPIO15 pins.

| Device  | <div style="width:100px">Config file</div> | Program                                |
| ------- | ------------------------------------------ | -------------------------------------- |
| PI Z/3B | test/rpiv3_bcm_in_out.cfg                  | test/v3_bcm_in_2_7_out_8_13.bas        |
| PI Z/3B | test/rpiv3_bcm_in_out.cfg                  | test/v3_bcm_in_16_21_out_22_27.bas     |
| PI Z/3B | test/rpiv3_bcm_out_in.cfg                  | test/v3_bcm_out_2_7_in_8_13.bas        |
| PI Z/3B | test/rpiv3_bcm_out_in.cfg                  | test/v3_bcm_out_16_21_in_22_17.bas     |
| PI Z/3B | test/rpiv3_bcm_ichigo.cfg                  | test/v3_ichigo.bas                     |
| PI Z/3B | test/rpiv3_board_in_out.cfg                | test/v3_board_in_3_13_out_15_22.bas    |
| PI Z/3B | test/rpiv3_board_in_out.cfg                | test/v3_board_in_23_32_out_33_40.bas   |
| PI Z/3B | test/rpiv3_board_out_in.cfg                | test/v3_board_out_3_13_in_15_22.bas    |
| PI Z/3B | test/rpiv3_board_out_in.cfg                | test/v3_board_out_23_32_in_33_40.bas   |
| PI Z/3B | test/rpiv3_board_ichigo.cfg                | test/v3_ichigo.bas                     |
| PI B    | test/rpiv1_bcm_in_out.cfg                  | test/v1_bcm_in_0_7_out_8_11.bas        |
| PI B    | test/rpiv1_bcm_in_out.cfg                  | test/v1_bcm_in_17_21_out_22_25.bas     |
| PI B    | test/rpiv1_bcm_out_in.cfg                  | test/v1_bcm_out_0_7_in_8_11.bas        |
| PI B    | test/rpiv1_bcm_out_in.cfg                  | test/v1_bcm_out_7_21_in_22_25.bas      |
| PI B    | test/rpiv1_bcm_ichigo.cfg                  | test/v1_2_ichigo.bas                   |
| PI B    | test/rpiv1_2_board_in_out.cfg              | test/v1_2_board_in_3_11_out_12_16.bas  |
| PI B    | test/rpiv1_2_board_in_out.cfg              | test/v1_2_board_in_18_21_out_22_26.bas |
| PI B    | test/rpiv1_2_board_out_in.cfg              | test/v1_2_board_out_3_11_in_12_16.bas  |
| PI B    | test/rpiv1_2_board_out_in.cfg              | test/v1_2_board_out_11_21_in_22_26.bas |
| PI B    | test/rpiv1_2_board_ichigo.cfg              | test/v1_2_ichigo.bas                   |

OUT_MODE examples (see also picture above): 

| Device  | <div style="width:100px">Config file</div> | Program              |
| ------- | ------------------------------------------ | -------------------- |
| PI Z/3B | test/rpiv3_bcm_out_mode_default.cfg        | test/v3_out_mode.bas |
| PI Z/3B | test/rpiv3_bcm_out_mode_0.cfg              | test/v3_out_mode.bas |
| PI Z/3B | test/rpiv3_bcm_out_mode_2.cfg              | test/v3_out_mode.bas |
| PI Z/3B | test/rpiv3_bcm_out_mode_defined.cfg        | test/v3_out_mode.bas |

ADC MCP3202 example

| Device  | <div style="width:100px">Config file</div> | Program      |
| ------- | ------------------------------------------ | ------------ |
| PI B    | test/rpiv1_2_board_ichigo_mcp3202.cfg      | test/adc.bas |
| PI Z/3B | test/test/rpiv3_bcm_ichigo_mcp3202.cfg     | test/adc.bas |

## Color code

  Check this file ([pdf](color.pdf), [ods](color.ods))  on how I defined the color codes.

  Supported color codes:

    BLACK
    BLUE
    BROWN
    CYAN
    DARKBLUE
    DARKGREEN
    DARKRED
    DARKVIOLET
    DEEPPINK
    GOLD
    GRAY
    GREEN
    LIME
    MAGENTA
    MAROON
    ORANGE
    ORANGERED
    PINK
    PURPLE
    RED
    VIOLET
    WHITE
    YELLOW

## Limitations:

* A line is up to 256 characters on basic side..
* Basic program size is limited to 8192 bytes. This is static allocated on "listbuf" (t_ttbasic.c), and on my system, it can be up to 32k (change SIZE_LIST in tanba.h).
