// Tanba Labs BASIC RPI Interface
//   Copyright (C) 2019  by the owner of tanba.labs @ gmail
//   Distributed under GPL v3


//  pigpio library
//    http://abyz.me.uk/rpi/pigpio/cif.html
//  simple code example
//    http://www.merkles.com/EmbeddedWorkshop/PiGPIO.html
//    https://elinux.org/RPi_GPIO_Code_Samples
//    http://abyz.me.uk/rpi/pigpio/examples.html
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pigpio.h>
#include "tanba.h"

// pin details
//  https://webofthings.org/2016/10/23/node-gpio-and-the-raspberry-pi/
//  https://raspi.tv/2014/rpi-gpio-quick-reference-updated-for-raspberry-pi-b
//  https://raspi.tv/2013/rpi-gpio-basics-4-setting-up-rpi-gpio-numbering-systems-and-inputs
//  https://www.raspberrypi.org/documentation/usage/gpio/

// http://abyz.me.uk/rpi/pigpio/index.html
// Type 1: Model B (original model)
//   26 pin header (P1).
//   Hardware revision numbers of 2 and 3.
//   User GPIO 0-1, 4, 7-11, 14-15, 17-18, 21-25.
// Type 2: Model A, B (revision 2)
//   26 pin header (P1) and an additional 8 pin header (P5).
//   Hardware revision numbers of 4, 5, 6 (B), 7, 8, 9 (A), and 13, 14, 15 (B).
//   User GPIO 2-4, 7-11, 14-15, 17-18, 22-25, 27-31.
// Type 3: Model A+, B+, Pi Zero, Pi2B, Pi3B
//   40 pin expansion header (J8).
//   Hardware revision numbers of 16 or greater.
//   User GPIO 2-27 (0 and 1 are reserved).

#define NUM_TYPES     3
#define NUM_USERGPIO 32
int gpioUserByType[NUM_TYPES][NUM_USERGPIO]=
 // 0                   1                   2                   3
 // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  {{1,1,0,0,1,0,0,1,1,1,1,1,0,0,1,1,0,1,1,0,0,1,1,1,1,1,0,0,0,0,0,0},   // type 1
   {0,0,1,1,1,0,0,1,1,1,1,1,0,0,1,1,0,1,1,0,0,0,1,1,1,1,0,1,1,1,1,1},   // type 2
   {0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}};  // type 3 (Ex: There's no GPIO0/1 for type 3)

// map between pin(BOARD) number and gpio(BCM)
int pinToGpio[NUM_TYPES][41]=
 //  0                          1                             2                          3                             4
 //  0  1  2 3  4 5  6 7  8  9  0  1  2  3  4  5  6  7  8  9  0 1  2  3 4  5 6  7  8  9  0  1  2  3  4  5  6  7  8  9  0
  {{-1,-1,-1,0,-1,1,-1,4,14,-1,15,17,18,21,-1,22,23,-1,24,10,-1,9,25,11,8,-1,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
   {-1,-1,-1,2,-1,3,-1,4,14,-1,15,17,18,27,-1,22,23,-1,24,10,-1,9,25,11,8,-1,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
   {-1,-1,-1,2,-1,3,-1,4,14,-1,15,17,18,27,-1,22,23,-1,24,10,-1,9,25,11,8,-1,7,-1,-1, 5,-1, 6,12,13,-1,19,16,26,20,-1,21}};

int type=0;

int adcHandler=0;

void initGpio(){
  if (gpioInitialise() < 0){
    printf("rpi.c pigpio initialisation failed\n");
    exit(-1);
  }
}

void terminateGpio(){
  gpioTerminate();
}

void initAdc(){
  // based on pigpio author on the following post
  //https://www.raspberrypi.org/forums/viewtopic.php?t=249872
  // see also
  // http://abyz.me.uk/rpi/pigpio/cif.html#spiOpen
  adcHandler= spiOpen(0, 1000000, 0);
  
}

void terminateAdc(){
  spiClose(adcHandler);
}

char *getHWName(){
  return "RPI";
}

int getGpioNum(int val){
  int gpioNum=-1;
  if (gpioMode==GPIO_BOARD) {
    if (val>0 && val<MAX_GPIO) {
      gpioNum=pinToGpio[type-1][val];
    }
  } else { // GPIO_BCM
    gpioNum=val;
  }
  return gpioNum;
}

int checkAndGetGpioNum(int port){
  if (type==2) { // NOT tested hardware
    return -1;
  }

  if (type<1 || type>3) { // unknown types
    return -1;
  }

  return getGpioNum(port);
}


char errcode[64];
char *getErrCode(int err){
  switch(err){
    case PI_BAD_GPIO      : strcpy(errcode,"PI_BAD_GPIO");       break;
    case PI_BAD_MODE      : strcpy(errcode,"PI_BAD_MODE");       break;
    case PI_BAD_LEVEL     : strcpy(errcode,"PI_BAD_LEVEL");      break;
    case PI_BAD_USER_GPIO : strcpy(errcode,"PI_BAD_USER_GPIO");  break;
    case PI_BAD_PUD       : strcpy(errcode,"PI_BAD_PUD");        break;
    case PI_BAD_DUTYCYCLE : strcpy(errcode,"PI_BAD_DUTYCYCLE");  break;
    case PI_BAD_PULSEWIDTH: strcpy(errcode,"PI_BAD_PULSEWIDTH"); break;
    default               : sprintf(errcode,"%d",err); break;
  }
  return errcode;
}

int userGpioStatus(int val){
  int gpioNum=getGpioNum(val);
  if (gpioNum<0) return 0;
  return gpioUserByType[type-1][gpioNum];
}


void  checkPort(){
  int v = gpioHardwareRevision();

  type = 0; // type 0 => could not determine.
  
  // https://www.raspberrypi-spy.co.uk/2012/09/checking-your-raspberry-pi-board-version/
  // http://abyz.me.uk/rpi/pigpio/index.html
  if (v==2 || v==3) {
    // Type 1: Model B (original model)
    //   26 pin header (P1).
    //   Hardware revision numbers of 2 and 3.
    //   User GPIO 0-1, 4, 7-11, 14-15, 17-18, 21-25.
    type=1;
  } else if ((v>=4 && v<=9) || (v>=13 && v<=15)) {
    // Type 2: Model A, B (revision 2)
    //   26 pin header (P1) and an additional 8 pin header (P5).
    //   Hardware revision numbers of 4, 5, 6 (B), 7, 8, 9 (A), and 13, 14, 15 (B).
    //   User GPIO 2-4, 7-11, 14-15, 17-18, 22-25, 27-31.
    type=2;

    printf("NOTE:  hardware revision %d (type 2) NOT TESTED! You need to comment a few lines to debug GPIO. See documents (wait 5  s) \n",v);
    sleep(5);
  } else if (v>=16) {
    // Type 3: Model A+, B+, Pi Zero, Pi2B, Pi3B
    //   40 pin expansion header (J8).
    //   Hardware revision numbers of 16 or greater.
    //   User GPIO 2-27 (0 and 1 are reserved).
    type=3;
  } else {
    printf("NOTE: Unknown RPI hardware revision %d\n",v);
    printf("      GPIO commands will not work\n");
    sleep(2);
    return;
  } 

  int num;
  for (num=0;num<MAX_GPIO;num++) {
    int portMode;
    for (portMode=0;portMode<4;portMode++) {
      int p;
      if        (portMode==IN_MODE) {
        p=gpioIn[num];
      } else if (portMode==OUT_MODE) {
        p=gpioOut[num];
      } else if (portMode==PWM_MODE) {
        p=gpioPwm[num];
      } else {          // SERVO_MODE
        p=gpioServo_[num];
      }
      if (p!=-1) {
        if (userGpioStatus(p)==0) {
          if (gpioMode==GPIO_BOARD) {
            printf("Config file ERROR: Specifed pin (%d) is not GPIO. Hardware revision:%d\n",num,v);
          } else {
            printf("Config file ERROR: (%d) is not user GPIO. Hardware revision:%d\n",num,v);
          } 
          exit(-1); 
        } else {
          int err;
          if (portMode==IN_MODE) {
            err=gpioSetMode(getGpioNum(p), PI_INPUT);
            if (err!=0) {
              printf("gpioSetMode error for input. Error %s. port %d  gpio %d\n",getErrCode(err),p,getGpioNum(p));
              exit(-1);
            }
            if (gpioInMode[num]!=PULLDEF) {
              err=gpioSetPullUpDown(getGpioNum(p), gpioInMode[num]);
              if (err!=0) {
                printf("gpioSetPullUpDown error. Error %s. port %d  gpio %d mode %d\n",getErrCode(err),p,getGpioNum(p),gpioInMode[num]);
                exit(-1);
              }
            }
          } else if (portMode==OUT_MODE || portMode==PWM_MODE || portMode==SERVO_MODE) {
            err=gpioSetMode(getGpioNum(p), PI_OUTPUT);
	    if (err!=0) {
              printf("gpioSetMode error for mode %d (output:1 pwm:2 servo:3). Error %s. port %d  gpio %d\n",portMode,getErrCode(err),p,getGpioNum(p));
              exit(-1);
	    }
          }
        } 
      } 
    } 
  } 
}


short getin(short port){
  int gpioNum=checkAndGetGpioNum(port);
  if(gpioNum<0) return 0;

  if (gpioUserByType[type-1][gpioNum]) { // if 0, not user GPIO
    int val=gpioRead(gpioNum);
    if (val==PI_BAD_GPIO) {
      logPrint("gpioRead PI_BAD_GPIO error. port %d  gpio %d\n",port,gpioNum);
      exit(-1);
    }
    return val;
  } else {
    logPrint("Not user gpio. IN has no effect. Port %d  gpio %d\n",port,gpioNum);
  }
  return 0;
}

void  setout(short port, short value){
  int gpioNum=checkAndGetGpioNum(port);
  if(gpioNum<0) return;

  if (gpioUserByType[type-1][gpioNum]) { // if 0, not user GPIO
    int err=gpioWrite(gpioNum, value>0);
    if (err!=0) {
      logPrint("gpioWrite error %s. port %d  gpio %d\n",getErrCode(err),port,gpioNum);
      exit(-1);
    }
  } else {
    logPrint("Not user gpio. OUT has no effect. Port %d  gpio %d\n",port,gpioNum);
  }
}

void  setpwm(short port, short value){
  int gpioNum=checkAndGetGpioNum(port);
  if(gpioNum<0) return;

  if (gpioUserByType[type-1][gpioNum]) { // if 0, not user GPIO
logPrint("rpi.c GPIO PWM NOT TESTED\n");
exit(-1);
    int err=gpioPWM(gpioNum, value);
    if (err!=0) {
      logPrint("gpioPWM error %s. port %d  gpio %d\n",getErrCode(err),port,gpioNum);
      exit(-1);
    }
  } else {
    logPrint("Not user gpio. PWM has no effect. Port %d  gpio %d\n",port,gpioNum);
  }
}

void  setservo(short port, short value){
  int gpioNum=checkAndGetGpioNum(port);
  if(gpioNum<0) return;
  
  if (gpioUserByType[type-1][gpioNum]) { // if 0, not user GPIO
logPrint("rpi.c GPIO PWM NOT TESTED\n");
exit(-1);
//    int err=gpioServo(gpioNum, value);
//    if (err!=0) {
//      logPrint("gpioServo error %s. port %d  gpio %d\n",getErrCode(err),port,gpioNum);
//      exit(-1);
//    }
  } else {
    logPrint("Not user gpio. SERVO has no effect. Port %d  gpio %d\n",port,gpioNum);
  }

}

short getana(short channel){


  if (adcModel<0 || channel>=AdcChannels[adcModel]) { // wrong channel
    return 0;
  }

  int bufData=AdcInitBuf[adcModel]; // initially, contains start bit and single mode
  bufData=bufData | (channel<<AdcChannelShift[adcModel]);

  unsigned char buf[3];
  buf[0] = (bufData>>16)&255;
  buf[1] = (bufData>> 8)&255;
  buf[2] = (bufData    )&255;
  spiXfer(adcHandler, buf, buf, AdcBytes[adcModel]); 

  short mask= (1<<AdcBits[adcModel])-1;
  short val = ((buf[AdcBytes[adcModel]-2]<<8) | buf[AdcBytes[adcModel]-1]) & mask;

  return val;
}

