// Tanba Labs BASIC IDE
//   Based on Antirez kilo editor (See notices below)
// 

/* Kilo -- A very simple editor in less than 1-kilo lines of code (as counted
 *         by "cloc"). Does not depend on libcurses, directly emits VT100
 *         escapes on the terminal.
 *
 * -----------------------------------------------------------------------
 *
 * Copyright (C) 2016 Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



/*** includes ***/

#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#include "tanba.h"

/*** defines ***/

#define TANBABASIC_VERSION "0.1"

#define CTRL_KEY(k) ((k) & 0x1f)

enum editorKey {
  BACKSPACE = 127,
  ARROW_LEFT = 1000,
  ARROW_RIGHT,
  ARROW_UP,
  ARROW_DOWN,
  DEL_KEY,
  HOME_KEY,
  END_KEY,
  PAGE_UP,
  PAGE_DOWN
};

enum editorHighlight {
  HL_NORMAL = 0,
  HL_COMMENT,
  HL_MLCOMMENT,
  HL_KEYWORD1,
  HL_KEYWORD2,
  HL_STRING,
  HL_NUMBER,
  HL_MATCH
};

#define HL_HIGHLIGHT_NUMBERS (1<<0)
#define HL_HIGHLIGHT_STRINGS (1<<1)

/*** data ***/

// row wrap on Tanba Labs version
//
//         text
//       0         1         2
//       012345678901234567890123456789
// cy wr+------------------------------+    'aaa', 'bbb' and 'ccc' are lines with len>screenrows
//  0  0|                              |
//  1  0|                              |
//  2  0|off aaaaaaaaaaaaaaaaaaaaaaaaaa|  off: rowoff and wrapoff
//  2  1+-a-a-a-a-a-a-a-a-a-a-a-a-a-a-p+-0--- screen from here
//  2  2|aaaaaaaa                      | 1
//  3  0|                              | 2
//  4  0|                              | 3
//  5  1|                              | 4
//  6  0|                              | 5
//  7  0|bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb| 6     ssy  : 6 (cursor row screen start y)
//  7  1|bbbbbbbbbbXbbb                | 7  X: cx/xy: 7,40
//  8  0|                              | 8
//  9  0|                              | 9
// 10  0|                              |10
// 11  0+------------------------------+11--- screen to here
// 12  0|                              |
// 13  0|                              |
// 14  0|                              |
// 15  0+------------------------------+ numcols=15 OR 16 (as original spec)


struct editorSyntax {
  char *filetype;
  char **filematch;
  char **keywords;
  char *singleline_comment_start;
  char *multiline_comment_start;
  char *multiline_comment_end;
  int flags;
};

typedef struct erow {
  int idx;
  int size;
  int rsize;
  char *chars;
  char *render;
  unsigned char *hl;
  int hl_open_comment;
} erow;

struct editorConfig {
  int cx, cy; // cursor x/y
  int rx;     // TO BE DELETED
  int rowoff;
  int coloff; // TO BE DELETED
  int wrapoff; // row wrap offset
  int ssy;     // cursor y start screen y
  int scx,scy; // screen cursor x/y
  int screenrows;
  int screencols;
  int numrows;
  erow *row;
  int dirty;
  char *filename;
  char statusmsg[80];
  time_t statusmsg_time;
  struct editorSyntax *syntax;
  struct termios orig_termios;
};

struct editorConfig E;

/*** filetypes ***/

char* BASIC_HL_extensions[] = {".bas", NULL};

char* BASIC_HL_keywords[] = {
  "GOTO", "GOSUB", "RETURN",
  "FOR", "TO", "STEP", "NEXT",
  "IF", "ELSE", "REM", "STOP",
  "INPUT", "PRINT", "LET",
  "RND", "ABS", "SIZE",
  "LIST", "RUN", "NEW", "SYSTEM",
  "?","WAIT","INKEY","CLS","COLOR","SGR",
  "YEAR","MONTH","DAY","HOUR","MINUTE","SECOND",
  "IN","OUT","LED","PWM","SERVO","GPIO",
  "LOCATE","LC","RESIZE","SAVE","LOAD","END",
  "goto", "gosub", "return",
  "for", "to", "step", "next",
  "if", "rem", "stop",
  "input", "print", "let",
  "rnd", "abs", "size",
  "list", "run", "new", "system",
  "wait","inkey","cls","color","sgr",
  "year","month","day","hour","minute","second",
  "in","out","led","pwm","servo","gpio","ana",
  "locate","lc","resize","save","load","end",
  NULL
};


struct editorSyntax HLDB[] = {
  {
    "basic",
    BASIC_HL_extensions,
    BASIC_HL_keywords,
    "REM",
    "REM",
    "REM",
    HL_HIGHLIGHT_NUMBERS | HL_HIGHLIGHT_STRINGS
  },
};

#define HLDB_ENTRIES (sizeof(HLDB) / sizeof(HLDB[0]))

int basicForegroundColor=-1;
int basicBackgroundColor=-1;
int sgrMode=256;


/*** debug ***/

void logPrint(char *fmt, ...){
  FILE *f_deb;
  f_deb=fopen("/tmp/tanba.log","a");
  if (f_deb==NULL) {
    fprintf(stderr,"Could not open debug file\n");
    exit(-1);
  }

  va_list ap;
  va_start(ap,fmt);
  vfprintf(f_deb,fmt,ap);
  va_end(ap);

  fclose(f_deb);
}

/*** terminal ***/

void die(const char *s) {
  write(STDOUT_FILENO, "\x1b[2J", 4);
  write(STDOUT_FILENO, "\x1b[H", 3);

  perror(s);
  exit(1);
}

void disableRawMode() {
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &E.orig_termios) == -1)
    die("tcsetattr");
}

void enableRawMode() {
  if (tcgetattr(STDIN_FILENO, &E.orig_termios) == -1) die("tcgetattr");
  atexit(disableRawMode);

  struct termios raw = E.orig_termios;
  raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
  raw.c_oflag &= ~(OPOST);
  raw.c_cflag |= (CS8);
  raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
  raw.c_cc[VMIN] = 0;
  raw.c_cc[VTIME] = 1;

  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) die("tcsetattr");
}

int editorReadKey() {
  int nread;
  char c;
  while ((nread = read(STDIN_FILENO, &c, 1)) != 1) {
    if (nread == -1 && errno != EAGAIN) die("read");
  }

  if (c == '\x1b') {
    char seq[3];

    if (read(STDIN_FILENO, &seq[0], 1) != 1) return '\x1b';
    if (read(STDIN_FILENO, &seq[1], 1) != 1) return '\x1b';

    if (seq[0] == '[') {
      if (seq[1] >= '0' && seq[1] <= '9') {
        if (read(STDIN_FILENO, &seq[2], 1) != 1) return '\x1b';
        if (seq[2] == '~') {
          switch (seq[1]) {
            case '1': return HOME_KEY;
            case '3': return DEL_KEY;
            case '4': return END_KEY;
            case '5': return PAGE_UP;
            case '6': return PAGE_DOWN;
            case '7': return HOME_KEY;
            case '8': return END_KEY;
          }
        }
      } else {
        switch (seq[1]) {
          case 'A': return ARROW_UP;
          case 'B': return ARROW_DOWN;
          case 'C': return ARROW_RIGHT;
          case 'D': return ARROW_LEFT;
          case 'H': return HOME_KEY;
          case 'F': return END_KEY;
        }
      }
    } else if (seq[0] == 'O') {
      switch (seq[1]) {
        case 'H': return HOME_KEY;
        case 'F': return END_KEY;
      }
    }

    return '\x1b';
  } else {
    return c;
  }
}

int getCursorPosition(int *rows, int *cols) {
  char buf[32];
  unsigned int i = 0;

  if (write(STDOUT_FILENO, "\x1b[6n", 4) != 4) return -1;

  while (i < sizeof(buf) - 1) {
    if (read(STDIN_FILENO, &buf[i], 1) != 1) break;
    if (buf[i] == 'R') break;
    i++;
  }
  buf[i] = '\0';

  if (buf[0] != '\x1b' || buf[1] != '[') return -1;
  if (sscanf(&buf[2], "%d;%d", rows, cols) != 2) return -1;

  return 0;
}

int getWindowSize(int *rows, int *cols) {
  struct winsize ws;

  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
    if (write(STDOUT_FILENO, "\x1b[999C\x1b[999B", 12) != 12) return -1;
    return getCursorPosition(rows, cols);
  } else {
    *cols = ws.ws_col;
    *rows = ws.ws_row;
    return 0;
  }
}

/*** syntax highlighting ***/

int is_separator(int c) {
  return isspace(c) || c == '\0' || strchr(",.()+-/*=~%<>[];", c) != NULL;
}

void editorUpdateSyntax(erow *row) {
  row->hl = realloc(row->hl, row->rsize);
  memset(row->hl, HL_NORMAL, row->rsize);
  if (E.syntax == NULL) return;

  char **keywords = E.syntax->keywords;

  char *scs = E.syntax->singleline_comment_start;
  char *mcs = E.syntax->multiline_comment_start;
  char *mce = E.syntax->multiline_comment_end;

  int scs_len = scs ? strlen(scs) : 0;
  int mcs_len = mcs ? strlen(mcs) : 0;
  int mce_len = mce ? strlen(mce) : 0;

  int prev_sep = 1;
  int in_string = 0;
  int in_comment = (row->idx > 0 && E.row[row->idx - 1].hl_open_comment);

  int i = 0;
  while (i < row->rsize) {
    char c = row->render[i];
    unsigned char prev_hl = (i > 0) ? row->hl[i - 1] : HL_NORMAL;

    if (scs_len && !in_string && !in_comment) {
      if (!strncmp(&row->render[i], scs, scs_len)) {
        memset(&row->hl[i], HL_COMMENT, row->rsize - i);
        break;
      }
    }

    if (mcs_len && mce_len && !in_string) {
      if (in_comment) {
        row->hl[i] = HL_MLCOMMENT;
        if (!strncmp(&row->render[i], mce, mce_len)) {
          memset(&row->hl[i], HL_MLCOMMENT, mce_len);
          i += mce_len;
          in_comment = 0;
          prev_sep = 1;
          continue;
        } else {
          i++;
          continue;
        }
      } else if (!strncmp(&row->render[i], mcs, mcs_len)) {
        memset(&row->hl[i], HL_MLCOMMENT, mcs_len);
        i += mcs_len;
        in_comment = 1;
        continue;
      }
    }

    if (E.syntax->flags & HL_HIGHLIGHT_STRINGS) {
      if (in_string) {
        row->hl[i] = HL_STRING;
        if (c == '\\' && i + 1 < row->rsize) {
          row->hl[i + 1] = HL_STRING;
          i += 2;
          continue;
        }
        if (c == in_string) in_string = 0;
        i++;
        prev_sep = 1;
        continue;
      } else {
        if (c == '"' || c == '\'') {
          in_string = c;
          row->hl[i] = HL_STRING;
          i++;
          continue;
        }
      }
    }

    if (E.syntax->flags & HL_HIGHLIGHT_NUMBERS) {
      if ((isdigit(c) && (prev_sep || prev_hl == HL_NUMBER)) ||
          (c == '.' && prev_hl == HL_NUMBER)) {
        row->hl[i] = HL_NUMBER;
        i++;
        prev_sep = 0;
        continue;
      }
    }

    if (prev_sep) {
      int j;
      for (j = 0; keywords[j]; j++) {
        int klen = strlen(keywords[j]);
        int kw2 = keywords[j][klen - 1] == '|';
        if (kw2) klen--;

        if (!strncmp(&row->render[i], keywords[j], klen) &&
            is_separator(row->render[i + klen])) {
          memset(&row->hl[i], kw2 ? HL_KEYWORD2 : HL_KEYWORD1, klen);
          i += klen;
          break;
        }
      }
      if (keywords[j] != NULL) {
        prev_sep = 0;
        continue;
      }
    }

    prev_sep = is_separator(c);
    i++;
  }

  int changed = (row->hl_open_comment != in_comment);
  row->hl_open_comment = in_comment;
  if (changed && row->idx + 1 < E.numrows)
    editorUpdateSyntax(&E.row[row->idx + 1]);
}

int editorSyntaxToColor(int hl) {
  switch (hl) {
    case HL_COMMENT:
    case HL_MLCOMMENT: return 36;
    case HL_KEYWORD1: return 33;
    case HL_KEYWORD2: return 32;
    case HL_STRING: return 35;
    case HL_NUMBER: return 31;
    case HL_MATCH: return 34;
    default: return 37;
  }
}

void editorSelectSyntaxHighlight() {
  E.syntax = NULL;
  struct editorSyntax *s = &HLDB[0];
  E.syntax = s;

  int filerow;
  for (filerow = 0; filerow < E.numrows; filerow++) {
    editorUpdateSyntax(&E.row[filerow]);
  }

  return;
}

/*** row operations ***/

int editorRowRxToCx(erow *row, int rx) {
  int cur_rx = 0;
  int cx;
  for (cx = 0; cx < row->size; cx++) {
    cur_rx++;

    if (cur_rx > rx) return cx;
  }
  return cx;
}

void editorUpdateRow(erow *row) {
  int j;

  free(row->render);
  row->render = malloc(row->size  + 1);

  int idx = 0;
  for (j = 0; j < row->size; j++) {
    row->render[idx++] = row->chars[j];
  }
  row->render[idx] = '\0';
  row->rsize = idx;
  editorUpdateSyntax(row);
}

void editorInsertRow(int at, char *s, size_t len) {
  if (at < 0 || at > E.numrows) return;

  E.row = realloc(E.row, sizeof(erow) * (E.numrows + 1));
  memmove(&E.row[at + 1], &E.row[at], sizeof(erow) * (E.numrows - at));
  for (int j = at + 1; j <= E.numrows; j++) E.row[j].idx++;

  E.row[at].idx = at;

  E.row[at].size = len;
  E.row[at].chars = malloc(len + 1);
  memcpy(E.row[at].chars, s, len);
  E.row[at].chars[len] = '\0';

  E.row[at].rsize = 0;
  E.row[at].render = NULL;
  E.row[at].hl = NULL;
  E.row[at].hl_open_comment = 0;
  editorUpdateRow(&E.row[at]);

  E.numrows++;
  E.dirty++;
}

void editorFreeRow(erow *row) {
  free(row->render);
  free(row->chars);
  free(row->hl);
}

void editorDelRow(int at) {
  if (at < 0 || at >= E.numrows) return;
  editorFreeRow(&E.row[at]);
  memmove(&E.row[at], &E.row[at + 1], sizeof(erow) * (E.numrows - at - 1));
  for (int j = at; j < E.numrows - 1; j++) E.row[j].idx--;
  E.numrows--;
  E.dirty++;
}

void editorRowInsertChar(erow *row, int at, int c) {
  if (at < 0 || at > row->size) at = row->size;
  row->chars = realloc(row->chars, row->size + 2);
  memmove(&row->chars[at + 1], &row->chars[at], row->size - at + 1);
  row->size++;
  row->chars[at] = c;
  editorUpdateRow(row);
  E.dirty++;
}

void editorRowAppendString(erow *row, char *s, size_t len) {
  row->chars = realloc(row->chars, row->size + len + 1);
  memcpy(&row->chars[row->size], s, len);
  row->size += len;
  row->chars[row->size] = '\0';
  editorUpdateRow(row);
  E.dirty++;
}

void editorRowDelChar(erow *row, int at) {
  if (at < 0 || at >= row->size) return;
  memmove(&row->chars[at], &row->chars[at + 1], row->size - at);
  row->size--;
  editorUpdateRow(row);
  E.dirty++;
}

/*** editor operations ***/

void editorInsertChar(int c) {
  if (E.cy == E.numrows) {
    editorInsertRow(E.numrows, "", 0);
  }
  editorRowInsertChar(&E.row[E.cy], E.cx, c);
  E.cx++;
}

void editorRowChar(erow* row, int at, int c) {
    if (at < 0 || at >= row -> size) { 
        at = row -> size;
        // We need to allocate 2 bytes because we also have to make room for
        // the null byte.
        row -> chars = realloc(row -> chars, row -> size + 2);
        row -> size++;
    }
    row -> chars[at] = c;
    editorUpdateRow(row);
    E.dirty++; // This way we can see "how dirty" a file is.
}

char cmdLine[SIZE_LINE];

void editorNewline() {
    if (E.row[E.cy].size!=0 &&
        E.cy<E.numrows) { // as in editorInserChars, E.cy==E.numrows means that we have nothing in this line.
      if (E.row[E.cy].size>=SIZE_LINE) {
        bufferError();
      } else {
        strncpy(cmdLine,E.row[E.cy].chars,E.row[E.cy].size);
        cmdLine[E.row[E.cy].size]=0;
        execBasic(cmdLine);
      }
    } else {
      editorInsertRow(E.cy, "", 0);
      E.ssy += E.row[E.cy].size/E.screencols + 1;
      E.cy++;
    }

    E.cx = 0;
}


void editorInsertNewline() {
  if (E.cy==(E.numrows-1)) {
    if (E.cx == 0) {
      editorInsertRow(E.cy, "", 0);
    } else {
      erow *row = &E.row[E.cy];
//      editorInsertRow(E.cy + 1, &row->chars[E.cx], row->size - E.cx);
      editorInsertRow(E.cy + 1, "", 0);
//      row = &E.row[E.cy];
//      row->size = E.cx;
//      row->chars[row->size] = '\0';
//      editorUpdateRow(row);
    }
  }
  E.cy++;E.ssy++;
  E.cx = 0;
}

void editorChar(int c) {
    // If this is true, the cursor is on the tilde line after the end of
    // the file, so we need to append a new row to the file before inserting
    // a character there.
    if (E.cy == E.numrows)
        editorInsertRow(E.numrows, "", 0);
    editorRowChar(&E.row[E.cy], E.cx, c);
    E.cx++; // This way we can see "how dirty" a file is.
}

void basicLine(char *line){
    int i;
    if (line!=NULL) {
      for (i=0;i<strlen(line);i++) {
        editorChar(line[i]);
      }
    }
}

void basicLocate(short xpos, short ypos){

// behaviour of the following is in editorScroll() 
//   E.cx    E.cy    
//   E.coloff  E.rowoff
//   E.screencols E.screenrows  => as the name, is screen number of rows


  if (ypos >= E.screenrows) ypos=E.screenrows-1; // we cannot start at outside screen.
  if (xpos >= E.screencols) xpos=E.screencols-1; // we cannot start at outside screen.
  int desired_y = E.rowoff + ypos;
  int desired_x = E.coloff + xpos;
  if (E.numrows<=desired_y) {
    E.cy=E.numrows-1;
    E.cx = E.row[E.cy].size;
    while(E.numrows<=desired_y) {
      editorInsertNewline();
    }
  }
  E.cy=desired_y;

  if (E.row[E.cy].size<desired_x) {
    while(E.row[E.cy].size<desired_x) {
      editorInsertChar(' ');
    }
  } else {
    E.cx=desired_x;
  }

  editorRefreshScreen();
}

void basicNewline(char *line) {
    basicLine(line);
    editorInsertNewline();
    editorRefreshScreen();
}

void basicCls(){
    while(E.numrows>1) {
      editorDelRow(0);
//      E.cy--;
    }
    E.cy=0;

}


void basicSetColor(int val1,int val2){
  E.syntax = NULL;
  if (sgrMode==256) { // 256 is highlight mode
    sgrMode=38; // 38 (set foreground color) is not used. If 38 directly set with basicFore/BackgroundColor
  }
  if (val1>=0) {
    basicForegroundColor=val1;
  }  
  if (val2>=0) {
    basicBackgroundColor=val2;
  }

}
  
void basicSetSGR(int val){
  sgrMode=val;

  if (val==256) { // highlight
    editorSelectSyntaxHighlight();
  } else {
    E.syntax = NULL;
    if (val==0) { // reset
      basicForegroundColor=-1;
      basicBackgroundColor=-1;
    }
    if (val>=30 && val<=37) {
      basicForegroundColor=val-30;
    }
    if (val==39) {
      basicForegroundColor=-1;
    }
    if (val>=90 && val<=97) {
      basicForegroundColor=val-90+8;
    }
    if (val>=40 && val<=47) {
      basicBackgroundColor=val-40;
    }
    if (val==49) {
      basicBackgroundColor=-1;
    }
    if (val>=100 && val<=107) {
      basicBackgroundColor=val-100+8;
    }
  } 
}

void basicScreenResize(int valw, int valh){
  char buf[16];
  int clen = clen= snprintf(buf, sizeof(buf), "\x1b[8;%d;%dt", valh,valw);
  write(STDOUT_FILENO, buf, clen);

  // https://stackoverflow.com/questions/48568875/terminal-window-size-doesnt-update-using-ioctl
  usleep(200000);
  basicCls();
  initEditor();  
}


void editorDelChar() {
  if (E.cy == E.numrows) return;
  if (E.cx == 0 && E.cy == 0) return;

  erow *row = &E.row[E.cy];
  if (E.cx > 0) {
    editorRowDelChar(row, E.cx - 1);
    E.cx--;
  } else {
    E.cx = E.row[E.cy - 1].size;
    editorRowAppendString(&E.row[E.cy - 1], row->chars, row->size);
    editorDelRow(E.cy);
    E.cy--;
  }
}

/*** file i/o ***/

char *editorRowsToString(int *buflen) {
  int totlen = 0;
  int j;
  for (j = 0; j < E.numrows; j++)
    totlen += E.row[j].size + 1;
  *buflen = totlen;

  char *buf = malloc(totlen);
  char *p = buf;
  for (j = 0; j < E.numrows; j++) {
    memcpy(p, E.row[j].chars, E.row[j].size);
    p += E.row[j].size;
    *p = '\n';
    p++;
  }

  return buf;
}

void editorOpen(char *filename) {
  free(E.filename);
  E.filename = strdup(filename);

  editorSelectSyntaxHighlight();

  FILE *fp = fopen(filename, "r");
  if (!fp) die("fopen");

  char *line = NULL;
  size_t linecap = 0;
  ssize_t linelen;
  while ((linelen = getline(&line, &linecap, fp)) != -1) {
    while (linelen > 0 && (line[linelen - 1] == '\n' ||
                           line[linelen - 1] == '\r'))
      linelen--;
    editorInsertRow(E.numrows, line, linelen);
  }
  free(line);
  fclose(fp);
  E.dirty = 0;
}

void editorSave() {
  if (E.filename == NULL) {
    E.filename = editorPrompt("Save as: %s (ESC to cancel)", NULL);
    if (E.filename == NULL) {
      editorSetStatusMessage("Save aborted");
      return;
    }
    editorSelectSyntaxHighlight();
  }

  int len;
  char *buf = editorRowsToString(&len);

  int fd = open(E.filename, O_RDWR | O_CREAT, 0644);
  if (fd != -1) {
    if (ftruncate(fd, len) != -1) {
      if (write(fd, buf, len) == len) {
        close(fd);
        free(buf);
        E.dirty = 0;
        editorSetStatusMessage("%d bytes written to disk", len);
        return;
      }
    }
    close(fd);
  }

  free(buf);
  editorSetStatusMessage("Can't save! I/O error: %s", strerror(errno));
}

/*** find ***/

void editorFindCallback(char *query, int key) {
  static int last_match = -1;
  static int direction = 1;

  static int saved_hl_line;
  static char *saved_hl = NULL;

  if (saved_hl) {
    memcpy(E.row[saved_hl_line].hl, saved_hl, E.row[saved_hl_line].rsize);
    free(saved_hl);
    saved_hl = NULL;
  }

  if (key == '\r' || key == '\x1b') {
    last_match = -1;
    direction = 1;
    return;
  } else if (key == ARROW_RIGHT || key == ARROW_DOWN) {
    direction = 1;
  } else if (key == ARROW_LEFT || key == ARROW_UP) {
    direction = -1;
  } else {
    last_match = -1;
    direction = 1;
  }

  if (last_match == -1) direction = 1;
  int current = last_match;
  int i;
  for (i = 0; i < E.numrows; i++) {
    current += direction;
    if (current == -1) current = E.numrows - 1;
    else if (current == E.numrows) current = 0;

    erow *row = &E.row[current];
    char *match = strstr(row->render, query);
    if (match) {
      last_match = current;
      E.cy = current;
      E.cx = editorRowRxToCx(row, match - row->render);
      E.rowoff = E.numrows;

      saved_hl_line = current;
      saved_hl = malloc(row->rsize);
      memcpy(saved_hl, row->hl, row->rsize);
      memset(&row->hl[match - row->render], HL_MATCH, strlen(query));
      break;
    }
  }
}

void editorFind() {
  int saved_cx = E.cx;
  int saved_cy = E.cy;
  int saved_coloff = E.coloff;
  int saved_rowoff = E.rowoff;

  char *query = editorPrompt("Search: %s (Use ESC/Arrows/Enter)",
                             editorFindCallback);

  if (query) {
    free(query);
  } else {
    E.cx = saved_cx;
    E.cy = saved_cy;
    E.coloff = saved_coloff;
    E.rowoff = saved_rowoff;
  }
}

/*** append buffer ***/

struct abuf {
  char *b;
  int len;
};

#define ABUF_INIT {NULL, 0}

void abAppend(struct abuf *ab, const char *s, int len) {
  char *new = realloc(ab->b, ab->len + len);

  if (new == NULL) return;
  memcpy(&new[ab->len], s, len);
  ab->b = new;
  ab->len += len;
}

void abFree(struct abuf *ab) {
  free(ab->b);
}

/*** output ***/

void editorScroll() {
  
  E.rx=E.cx;
  if (E.cy==E.numrows) { // When E.cy==E.numrows, E.cx is UNKNOWN!!! 
    E.rx=0; // Basicaly, E.rx IS E.cx.
  }

  if (E.cy < E.rowoff) {
    E.rowoff = E.cy;
  }
  if (E.cy >= E.rowoff + E.screenrows) {
    E.rowoff = E.cy - E.screenrows + 1;
  }


}

void editorDrawRows(struct abuf *ab) {


  int y;
  int sx=0; // screen x position
  int sy=0; 
  for (y = 0; y < E.screenrows; y++) {
    int filerow = y + E.rowoff;
    if (filerow >= E.numrows) {
      if (E.numrows == 0 && y == E.screenrows / 3) { // Show welcome 
        char welcome[80];
        int welcomelen = snprintf(welcome, sizeof(welcome),
          "Tanba BASIC -- version %s", TANBABASIC_VERSION);
        if (welcomelen > E.screencols) welcomelen = E.screencols;
        int padding = (E.screencols - welcomelen) / 2;
        while (padding--) abAppend(ab, " ", 1);
        abAppend(ab, welcome, welcomelen);
      } else {
      }
    } else {

      int len = E.row[filerow].rsize; // DELETE coloff - E.coloff;

      char *c = E.row[filerow].render; // DELETE [E.coloff];
      unsigned char *hl = E.row[filerow].hl; // DELETE [E.coloff];
      int current_color = -1;
      int j;

      int start_wrap_x = E.wrapoff * E.screencols;
      for (j = 0; j < len; j++) { // each char in one row
        if (y>0 || j>=start_wrap_x) {
          int wx=j%E.screencols; // wrapped x
          int wy=j/E.screencols;
          if (wy>0 && wx==0 && (y>0 || j>start_wrap_x)) { // new wrapped line.
            sy++;
            if (sy==E.screencols) break;
            abAppend(ab, "\x1b[39m", 5); // default fore color
            abAppend(ab, "\x1b[K", 3);   // erase line, from cursor
            abAppend(ab, "\r\n", 2);
          }
          //
          if (iscntrl(c[j])) {
            char sym = (c[j] <= 26) ? '@' + c[j] : '?';
            abAppend(ab, "\x1b[7m", 4); // reverse video
            abAppend(ab, &sym, 1);
            abAppend(ab, "\x1b[m", 3);
            if (current_color != -1) {
              char buf[16];
              int clen = snprintf(buf, sizeof(buf), "\x1b[%dm", current_color);
              abAppend(ab, buf, clen);
            }
          } else if (hl[j] == HL_NORMAL || sgrMode!=256) {

            if (E.syntax == NULL) { // no syntax highlight
              if (j==0) {
                char buf[16];
                int clen;
                if (sgrMode!=38 && sgrMode!=48) { // these are done below.
                  clen= snprintf(buf, sizeof(buf), "\x1b[%dm", sgrMode);
                  abAppend(ab, buf, clen);
                }		
                if (basicForegroundColor>=0) {
                  clen = snprintf(buf, sizeof(buf), "\x1b[38;5;%dm", basicForegroundColor);
                  abAppend(ab, buf, clen);
                }
                if (basicBackgroundColor>=0) {
                  clen = snprintf(buf, sizeof(buf), "\x1b[48;5;%dm", basicBackgroundColor);
                  abAppend(ab, buf, clen);
                }
              }
            } else {
              if (current_color != -1) {
                abAppend(ab, "\x1b[39m", 5);
                current_color = -1;
              }
            }

            abAppend(ab, &c[j], 1);
          } else {
            int color = editorSyntaxToColor(hl[j]);
            if (color != current_color) {
              current_color = color;
              char buf[16];
              int clen = snprintf(buf, sizeof(buf), "\x1b[%dm", color);
              abAppend(ab, buf, clen);
            }
            abAppend(ab, &c[j], 1);
          }

          // Can we improve this code?
          if (E.cy==filerow && (E.cx==j || 
                               (E.cx==len && E.cx==(j+1)))) { // when cursor points to last char, cx==len...
            E.scx = wx;
            E.scy = sy;
            if (E.cx==len){ 
              E.scx++;
              if (E.scx==E.screencols) {
                E.scx=0;
                E.scy++;
                // new wrap line...
                sy++;
                abAppend(ab, "\x1b[39m", 5);
                abAppend(ab, "\x1b[K", 3);
                abAppend(ab, "\r\n", 2);
                if (sy==E.screencols) break;
              }
            }
          }



        }
      } // j

      if (len==0) {
        if (E.syntax == NULL) { // no syntax highlight
          if (basicBackgroundColor>=0) {
            char buf[16];
            int clen;
            clen = snprintf(buf, sizeof(buf), "\x1b[48;5;%dm", basicBackgroundColor);
            abAppend(ab, buf, clen);
          }
       }
     }


      if (E.cy==filerow && len==0) {
        E.scx = 0;
        E.scy = sy;
      }

      if (sy==E.screencols) break;
      abAppend(ab, "\x1b[39m", 5);
    }

    abAppend(ab, "\x1b[K", 3);
    abAppend(ab, "\r\n", 2);
    sy++;
    if (sy==E.screenrows) break;
  } // y
}

void editorDrawStatusBar(struct abuf *ab) {
  abAppend(ab, "\x1b[7m", 4);
  char status[80], rstatus[80];
  int len = 0;//snprintf(status, sizeof(status), "%.20s - %d lines %s",    E.filename ? E.filename : "[No Name]", E.numrows,    E.dirty ? "(modified)" : "");
  int rlen = snprintf(rstatus, sizeof(rstatus), "%s | %d/%d",
    E.syntax ? E.syntax->filetype : "no ft", E.cy + 1, E.numrows);
  if (len > E.screencols) len = E.screencols;
  abAppend(ab, status, len);
  while (len < E.screencols) {
    if (E.screencols - len == rlen) {
      abAppend(ab, rstatus, rlen);
      break;
    } else {
      abAppend(ab, " ", 1);
      len++;
    }
  }
  abAppend(ab, "\x1b[m", 3);
  abAppend(ab, "\r\n", 2);
}

void editorDrawMessageBar(struct abuf *ab) {
  abAppend(ab, "\x1b[K", 3);
  int msglen = strlen(E.statusmsg);
  if (msglen > E.screencols) msglen = E.screencols;
  if (msglen && time(NULL) - E.statusmsg_time < 5)
    abAppend(ab, E.statusmsg, msglen);
}

void editorRefreshScreen() {
  editorScroll();

  struct abuf ab = ABUF_INIT;

  abAppend(&ab, "\x1b[?25l", 6);
  abAppend(&ab, "\x1b[H", 3);

  editorDrawRows(&ab);
  editorDrawStatusBar(&ab);
  editorDrawMessageBar(&ab);

  char buf[32];
  //snprintf(buf, sizeof(buf), "\x1b[%d;%dH", (E.cy - E.rowoff) + 1, (E.rx - E.coloff) + 1);
  snprintf(buf, sizeof(buf), "\x1b[%d;%dH", E.scy + 1, E.scx + 1);
  abAppend(&ab, buf, strlen(buf));

  abAppend(&ab, "\x1b[?25h", 6); // show the cursor

  write(STDOUT_FILENO, ab.b, ab.len);
  abFree(&ab);
}

void editorSetStatusMessage(const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vsnprintf(E.statusmsg, sizeof(E.statusmsg), fmt, ap);
  va_end(ap);
  E.statusmsg_time = time(NULL);
}

/*** input ***/

char *editorPrompt(char *prompt, void (*callback)(char *, int)) {
  size_t bufsize = 128;
  char *buf = malloc(bufsize);

  size_t buflen = 0;
  buf[0] = '\0';

  while (1) {
    editorSetStatusMessage(prompt, buf);
    editorRefreshScreen();

    int c = editorReadKey();
    if (c == DEL_KEY || c == CTRL_KEY('h') || c == BACKSPACE) {
      if (buflen != 0) buf[--buflen] = '\0';
    } else if (c == '\x1b') {
      editorSetStatusMessage("");
      if (callback) callback(buf, c);
      free(buf);
      return NULL;
    } else if (c == '\r') {
      if (buflen != 0) {
        editorSetStatusMessage("");
        if (callback) callback(buf, c);
        return buf;
      }
    } else if (!iscntrl(c) && c < 128) {
      if (buflen == bufsize - 1) {
        bufsize *= 2;
        buf = realloc(buf, bufsize);
      }
      buf[buflen++] = c;
      buf[buflen] = '\0';
    }

    if (callback) callback(buf, c);
  }
}

void editorMoveCursor(int key) {
  erow *row = (E.cy >= E.numrows) ? NULL : &E.row[E.cy];

  int num_wrap_rows;
  int new_cx;

  switch (key) {
    case ARROW_LEFT:
      if (E.cx != 0) {
        E.cx--;
      } else if (E.cy > 0) {
        E.cy--;
        E.cx = E.row[E.cy].size;
      }
      break;
    case ARROW_RIGHT:
      if (row && E.cx < row->size) {
        E.cx++;
      } else if (row && E.cx == row->size) {
        E.cy++;
        E.cx = 0;
      }
      break;
    case ARROW_UP:
      if (E.cx>=E.screencols) { // same row. Go up same row
        E.cx=E.cx-E.screencols;
      } else if (E.cy>0) { // diff row. Go up one row
        E.cy--;
        num_wrap_rows=E.row[E.cy].rsize/E.screencols;
        new_cx=num_wrap_rows*E.screencols+E.scx;
        if (new_cx >= E.row[E.cy].rsize) new_cx=E.row[E.cy].rsize;
        E.cx=new_cx;
      }
      break;
    case ARROW_DOWN:
      num_wrap_rows=E.row[E.cy].rsize/E.screencols;
      if (E.cy < E.numrows) {  // NOTE: when E.cy==E.numrows, E.cx is UNKNOWN (Should be ZERO)...
        if ( E.cx < (num_wrap_rows*E.screencols)) { // can wrap same row
          E.cx += E.screencols;
          if (E.scx > (E.row[E.cy].rsize%E.screencols)) {
            E.scx = E.row[E.cy].rsize%E.screencols;
            E.cx  = E.row[E.cy].rsize;
          } 
        } else {
          E.cy++;
          E.cx=E.scx;
          if (E.cy < E.numrows && E.scx>E.row[E.cy].rsize) {
            E.cx =E.row[E.cy].rsize;
            E.scx=E.row[E.cy].rsize;
          }
        }
        // The following is to adjust scy for last row... could not find a more elegant way to do it.
        E.scy++;
        if (E.scy==E.screenrows) E.scy=E.screenrows-1;
      }
      break;
  }

  row = (E.cy >= E.numrows) ? NULL : &E.row[E.cy];
  int rowlen = row ? row->size : 0;
  if (E.cx > rowlen) {
    E.cx = rowlen;
  }
}

void editorProcessKeypress() {

  int c = editorReadKey();
  int num_spaces;

  switch (c) {
    case '\r':
            editorNewline();
      break;

    case '\t': // TAB
      num_spaces=E.screencols-(E.cx%E.screencols);	
      if (num_spaces>=8) num_spaces=8;
      if (num_spaces>0) {
        while(num_spaces--) {
          editorInsertChar(' ');
        }
      }
      break;	
    case CTRL_KEY('q'):
      write(STDOUT_FILENO, "\x1b[2J", 4); // clear entire(2) screen
      write(STDOUT_FILENO, "\x1b[H", 3);  // Cursor position 
      exit(0);
      break;

    case CTRL_KEY('s'):
      editorSave();
      break;

    case HOME_KEY:
      E.cx = 0;
      break;

    case END_KEY:
      if (E.cy < E.numrows)
        E.cx = E.row[E.cy].size;
      break;

    case CTRL_KEY('f'):
      editorFind();
      break;

    case BACKSPACE:
    case CTRL_KEY('h'):
    case DEL_KEY:
      if (c == DEL_KEY) editorMoveCursor(ARROW_RIGHT);
      editorDelChar();
      break;

    case ARROW_UP:
    case ARROW_DOWN:
    case ARROW_LEFT:
    case ARROW_RIGHT:
      editorMoveCursor(c);
      break;

    case CTRL_KEY('l'):
    case '\x1b':
      break;

    default:
      editorInsertChar(c);
      break;
  }

}

/*** init ***/

void initEditor() {
  E.cx = 0;
  E.cy = 0;
  E.rx = 0;
  E.rowoff = 0;
  E.coloff = 0;
  E.wrapoff = 0;
  E.ssy = 0;
  E.scx = 0;
  E.scy = 0;
  E.numrows = 0;
  E.row = NULL;
  E.dirty = 0;
  E.filename = NULL;
  E.statusmsg[0] = '\0';
  E.statusmsg_time = 0;
  E.syntax = NULL;

  if (getWindowSize(&E.screenrows, &E.screencols) == -1) die("getWindowSize");
  E.screenrows -= 2;
}

int main(int argc, char *argv[]) {
  char cfgFileName[256];
  if (argc >= 2) {
    strcpy(cfgFileName,argv[1]);
  } else {
#if defined(RPI)
    printf("Usage for RPI: %s <gpio config file name>\n",argv[0]);
    exit(-1);
#else    
    strcpy(cfgFileName,"tanba.cfg");
#endif    
  }
  initBasic(cfgFileName);

  enableRawMode();
  initEditor();

  editorSetStatusMessage(" Ctrl-Q to quit | Ctrl-S to save screen lines");

  editorSelectSyntaxHighlight();
  initBasic(NULL);

  while (1) {
    editorRefreshScreen();
    editorProcessKeypress();
  }
  terminateBasic();

  return 0;
}
