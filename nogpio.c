// Tanba Labs BASIC no GPIO Interface
//   Copyright (C) 2019  by the owner of tanba.labs @ gmail
//   Distributed under GPL v3

#include "tanba.h"

void initGpio(){
}

void terminateGpio(){
}

void initAdc(){
}

void terminateAdc(){
}

char *getHWName(){
  return "NoGPIO";
}

short getin(short value){
  return 0;
}

void checkPort(){
}

void  setout(short port, short value){
  // do nothing
}

void  setpwm(short port, short value){
  // do nothing
}

void  setservo(short port, short value){
  // do nothing
}

short getana(short channel){
  return 0;
}
