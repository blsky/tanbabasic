// Tanba Labs BASIC

#ifndef __TANBA_BASIC__
#define __TANBA_BASIC__

/*** editor prototypes ***/

void initEditor();
void disableRawMode();
void enableRawMode();
void editorSetStatusMessage(const char *fmt, ...);
void editorRefreshScreen();
char *editorPrompt(char *prompt, void (*callback)(char *, int));
void basicLine(char *line);
void basicLocate(short xpos, short ypos);
void basicNewline(char *line);
void basicCls();
void basicSetColor(int val1,int val2);
void basicSetSGR(int val);
void basicScreenResize(int valw,int valh);
void editorDelChar();

// basic prototypes
void initBasic(char *cfgFile);
void terminateBasic();
void execBasic(char *cmd);
void bufferError();

// gpio
#define GPIO_BOARD 1
#define GPIO_BCM   2
extern int gpioMode; // one of the above modes

extern int outMode; // output mode

#define MAX_GPIO 41
#define PULLDEF  -1
#define PULLOFF   0 // numbers as in pigpio. See:
#define PULLUP    1 //     /usr/include/pigpio.h
#define PULLDOWN  2 //     http://abyz.me.uk/rpi/pigpio/cif.html#gpioSetPullUpDown

#define IN_MODE    0
#define OUT_MODE   1
#define PWM_MODE   2
#define SERVO_MODE 3


extern int gpioIn[    MAX_GPIO];
extern int gpioInMode[MAX_GPIO]; // one of the above mode
extern int gpioOut[   MAX_GPIO];
extern int gpioPwm[   MAX_GPIO];
extern int gpioServo_[ MAX_GPIO];

extern int numAdcMainChannels;
extern int numAdcMainBits;

// Basic
#define SIZE_LINE 256  // Command line buffer length + NULL
#define SIZE_IBUF 256  // i-code conversion buffer size
#define SIZE_LIST 8192 // List buffer size
#define SIZE_ARRY 64   // Array area size
#define SIZE_GSTK 9    // GOSUB stack size(3/nest)
#define SIZE_LSTK 15   // FOR stack size(5/nest)

#define NUM_ADC_MODELS 6
#define MCP3002 0
#define MCP3004 1
#define MCP3008 2
#define MCP3202 3
#define MCP3204 4
#define MCP3208 5

extern int adcModel;
extern int AdcBytes[       NUM_ADC_MODELS];
extern int AdcBits[        NUM_ADC_MODELS];
extern int AdcInitBuf[     NUM_ADC_MODELS];
extern int AdcChannels[    NUM_ADC_MODELS];
extern int AdcChannelShift[NUM_ADC_MODELS];


void initGpio();
void terminateGpio();

void initAdc();
void terminateAdc();

char *getHWName();
void  checkPort();
short getin(short value);
void  setout(short port, short value);
void  setpwm(short port, short value);
void  setservo(short port, short value);
short getana(short channel);

// debug
void logPrint(char *fmt, ...);


#endif
